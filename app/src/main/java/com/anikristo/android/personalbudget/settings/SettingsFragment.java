package com.anikristo.android.personalbudget.settings;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import com.anikristo.android.personalbudget.R;
import com.anikristo.android.personalbudget.dialogs.CurrencyPickerDialog;
import com.anikristo.android.personalbudget.misc.Currency;
import com.anikristo.android.personalbudget.misc.CurrencyHolder;

/**
 * TODO
 */
public class SettingsFragment extends PreferenceFragment implements
        Preference.OnPreferenceChangeListener,
        Preference.OnPreferenceClickListener {

    // PREFERENCES
    private Preference currencyValue;

    private SharedPreferences pref;

    // METHODS
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref);

        pref = PreferenceManager.getDefaultSharedPreferences(getActivity());

        CheckBoxPreference currencyMode = (CheckBoxPreference) findPreference(getString(R.string.pref_key_currency_mode));
        currencyMode.setOnPreferenceChangeListener(this);

        currencyValue = findPreference(getString(R.string.pref_key_currency_value));
        currencyValue.setEnabled(!pref.getBoolean(currencyMode.getKey(), false));
        currencyValue.setOnPreferenceClickListener(this);
        Currency currency = CurrencyHolder.getCurrency(pref.getInt(getResources()
                        .getString(R.string.pref_key_currency_value),
                CurrencyHolder.getPosition(CurrencyHolder.getDefaultCurrency(getActivity()))));
        currencyValue.setSummary(currency.getFullName() + " (" + currency.getAbbreviation() + ")");
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if (preference.getKey().equals(getString(R.string.pref_key_currency_mode))) {
            // hide or show the Currency Value Preference screen
            currencyValue.setEnabled(!(Boolean) newValue);

            // Change display
            Currency currency = CurrencyHolder.getDefaultCurrency(getActivity());
            currencyValue.setSummary(currency.getFullName() + " (" + currency.getAbbreviation() + ")");

            // Change value
            SharedPreferences.Editor editor = pref.edit();
            editor.putInt(getString(R.string.pref_key_currency_value), CurrencyHolder.getPosition(currency));
            editor.apply();

            return true;
        }
        return false;
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        if (preference.getKey().equals(getString(R.string.pref_key_currency_value))) {
            // Display currency picker dialog
            new CurrencyPickerDialog().show(getFragmentManager(), "CURRENCY_DIALOG");
        }

        return true;
    }

    void setCurrency(int position) {
        // Preference view
        Currency currency = CurrencyHolder.getCurrency(position);
        currencyValue.setSummary(currency.getFullName() + " (" + currency.getAbbreviation() + ")");

        // SharedPreferences
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(getString(R.string.pref_key_currency_value), position);
        editor.apply();
    }
}
