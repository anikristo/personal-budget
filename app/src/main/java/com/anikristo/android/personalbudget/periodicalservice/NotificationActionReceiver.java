package com.anikristo.android.personalbudget.periodicalservice;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.anikristo.android.personalbudget.R;
import com.anikristo.android.personalbudget.database.DBContract;
import com.anikristo.android.personalbudget.database.DBHelper;
import com.anikristo.android.personalbudget.misc.CategoryHolder;
import com.anikristo.android.personalbudget.misc.CurrencyHolder;
import com.anikristo.android.personalbudget.misc.Utils;

import java.util.Calendar;

/**
 * TODO
 */
public class NotificationActionReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        DBHelper helper = new DBHelper(context);

        String responseType = intent.getAction();

        Bundle extras = intent.getExtras();
        int recordType = extras.getInt(Utils.INTENT_EXTRA_RECORD_TYPE, DBContract.RECORD_INCOME);
        long rowID = extras.getLong(Utils.INTENT_EXTRA_ROW_ID, 0xFFFFFF);

        if (responseType.equals(Utils.INTENT_ACTION_NOTIFICATION_CONFIRM)) {

            // Getting the properties
            Double amount = extras.getDouble(Utils.INTENT_EXTRA_AMOUNT);
            Integer currency = extras.getInt(Utils.INTENT_EXTRA_CURRENCY);
            Integer period = extras.getInt(Utils.INTENT_EXTRA_PERIOD);
            String notes = extras.getString(Utils.INTENT_EXTRA_NOTES);
            Integer category = extras.getInt(Utils.INTENT_EXTRA_CATEGORY);

            Calendar c = Calendar.getInstance();
            int day = c.get(Calendar.DAY_OF_MONTH);
            int month = c.get(Calendar.MONTH) + 1;
            int year = c.get(Calendar.YEAR);
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Inserting
            if (recordType == DBContract.RECORD_INCOME)
                helper.insertIncomeRecord(
                        DBContract.INSERTION_TYPE_AUTO,
                        amount,
                        CurrencyHolder.getCurrency(currency),
                        CategoryHolder.getCategory(category, recordType),
                        period,
                        day,
                        month,
                        year,
                        hour,
                        minute,
                        notes
                );
            else
                helper.insertExpenseRecord(
                        DBContract.INSERTION_TYPE_AUTO,
                        amount,
                        CurrencyHolder.getCurrency(currency),
                        CategoryHolder.getCategory(category, recordType),
                        period,
                        day,
                        month,
                        year,
                        hour,
                        minute,
                        notes
                );

            // TODO update notification
            Toast.makeText(context, R.string.info_record_added, Toast.LENGTH_SHORT).show();

            NotificationManager mng = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mng.cancel(recordType == DBContract.RECORD_INCOME ? Utils.NOTIFICATION_ID_INCOME : Utils.NOTIFICATION_ID_EXPENSE);

        } else if (responseType.equals(Utils.INTENT_ACTION_NOTIFICATION_CANCEL)) {

            ((AlarmManager) context.getSystemService(Context.ALARM_SERVICE))
                    .cancel(
                            PendingIntent.getBroadcast(
                                    context,
                                    (int) rowID,
                                    new Intent(Utils.INTENT_ACTION_PERIODICAL_SERVICE),
                                    PendingIntent.FLAG_UPDATE_CURRENT)
                    );


            helper.unregisterAlarm(rowID);

            // TODO update notification
            Toast.makeText(context, R.string.info_periodical_record_cancelled, Toast.LENGTH_SHORT).show();

            NotificationManager mng = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mng.cancel(recordType == DBContract.RECORD_INCOME ? Utils.NOTIFICATION_ID_INCOME : Utils.NOTIFICATION_ID_EXPENSE);
        }
    }
}
