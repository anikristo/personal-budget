package com.anikristo.android.personalbudget.periodicalservice;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.NotificationCompat;

import com.anikristo.android.personalbudget.R;
import com.anikristo.android.personalbudget.database.DBContract;
import com.anikristo.android.personalbudget.database.DBHelper;
import com.anikristo.android.personalbudget.main.MainActivity;
import com.anikristo.android.personalbudget.misc.CategoryHolder;
import com.anikristo.android.personalbudget.misc.CurrencyHolder;
import com.anikristo.android.personalbudget.misc.Utils;

import java.util.Calendar;

/**
 * TODO
 */
public class PeriodicalServiceReceiver extends BroadcastReceiver {

    // METHODS
    @Override
    public void onReceive(Context context, Intent intent) {

        // Reading preferences to know whether the insertion should be confirmed first or not
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean addAuto = prefs.getBoolean(context.getString(R.string.pref_key_periodical_confirmation), true);

        // Getting the properties
        Bundle extras = intent.getExtras();
        int recordType = extras.getInt(Utils.INTENT_EXTRA_RECORD_TYPE, DBContract.RECORD_INCOME);

        Double amount = extras.getDouble(Utils.INTENT_EXTRA_AMOUNT);
        Integer currency = extras.getInt(Utils.INTENT_EXTRA_CURRENCY);
        Integer period = extras.getInt(Utils.INTENT_EXTRA_PERIOD);
        Integer day = extras.getInt(Utils.INTENT_EXTRA_DAY);
        Integer month = extras.getInt(Utils.INTENT_EXTRA_MONTH);
        Integer year = extras.getInt(Utils.INTENT_EXTRA_YEAR);
        String notes = extras.getString(Utils.INTENT_EXTRA_NOTES);
        Integer category = extras.getInt(Utils.INTENT_EXTRA_CATEGORY);
        long rowID = extras.getLong(Utils.INTENT_EXTRA_ROW_ID);

        if (addAuto) {

            // Inserting
            Calendar c = Calendar.getInstance();
            day = c.get(Calendar.DAY_OF_MONTH);
            month = c.get(Calendar.MONTH) + 1;
            year = c.get(Calendar.YEAR);

            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            DBHelper helper = new DBHelper(context);
            if (recordType == DBContract.RECORD_INCOME)
                helper.insertIncomeRecord(
                        DBContract.INSERTION_TYPE_AUTO,
                        amount,
                        CurrencyHolder.getCurrency(currency),
                        CategoryHolder.getCategory(category, recordType),
                        period,
                        day,
                        month,
                        year,
                        hour,
                        minute,
                        notes
                );
            else
                helper.insertExpenseRecord(
                        DBContract.INSERTION_TYPE_AUTO,
                        amount,
                        CurrencyHolder.getCurrency(currency),
                        CategoryHolder.getCategory(category, recordType),
                        period,
                        day,
                        month,
                        year,
                        hour,
                        minute,
                        notes
                );

            // Checking shared prefs for notifications
            boolean showNotification = prefs.getBoolean(context.getString(R.string.pref_key_periodical_notification), true);

            if (showNotification) {

                // Preparing notification
                Intent wrappingIntent = new Intent(context, MainActivity.class);
                wrappingIntent.setAction(Utils.INTENT_ACTION_NOTIFICATION);
                wrappingIntent.putExtra(Utils.INTENT_EXTRA_RECORD_TYPE, recordType);
                wrappingIntent.putExtra(Utils.INTENT_EXTRA_ORIGIN, Utils.INTENT_EXTRA_ORIGIN_NOTIFICATION_AUTO_ADD);

                PendingIntent contentIntent = PendingIntent.getActivity(
                        context,
                        0,
                        wrappingIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

                String message = Utils.getNotificationMessage(context, false, recordType, amount, currency, day, month, year);

                NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
                builder.setPriority(NotificationCompat.PRIORITY_LOW);
                builder.setSmallIcon(R.drawable.ic_launcher); // TODO
                builder.setDefaults(NotificationCompat.DEFAULT_SOUND | NotificationCompat.DEFAULT_LIGHTS);
                builder.setContentTitle(recordType ==
                                DBContract.RECORD_INCOME ?
                                context.getString(R.string.title_notification_new_income)
                                : context.getString(R.string.title_notification_new_expense)
                );
                builder.setTicker(recordType ==
                                DBContract.RECORD_INCOME ?
                                context.getString(R.string.title_notification_new_income)
                                : context.getString(R.string.title_notification_new_expense)
                );
                builder.setContentText(message);
                builder.setAutoCancel(true);
                builder.setVisibility(NotificationCompat.VISIBILITY_PRIVATE);

                // Sending notification
                builder.setContentIntent(contentIntent);
                notificationManager.notify(0x77, builder.build()); // Random ID
            }

        } else { // Confirmation needed

            Intent wrappingIntent = new Intent(context, MainActivity.class);
            wrappingIntent.setAction(Utils.INTENT_ACTION_NOTIFICATION);
            wrappingIntent.putExtra(Utils.INTENT_EXTRA_ORIGIN, Utils.INTENT_EXTRA_ORIGIN_NOTIFICATION_CONFIRM);
            wrappingIntent.putExtras(extras);

            PendingIntent contentIntent = PendingIntent.getActivity(
                    context,
                    0,
                    wrappingIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT
            );

            // Preparing notification
            String message = Utils.getNotificationMessage(context, true, recordType, amount, currency, day, month, year);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
            builder.setPriority(NotificationCompat.PRIORITY_LOW);
            builder.setSmallIcon(R.drawable.ic_launcher); // TODO
            builder.setDefaults(NotificationCompat.DEFAULT_SOUND | NotificationCompat.DEFAULT_LIGHTS);
            builder.setContentTitle(recordType ==
                            DBContract.RECORD_INCOME ?
                            context.getString(R.string.title_notification_new_income)
                            : context.getString(R.string.title_notification_new_expense)
            );
            builder.setTicker(recordType ==
                            DBContract.RECORD_INCOME ?
                            context.getString(R.string.title_notification_new_income)
                            : context.getString(R.string.title_notification_new_expense)
            );
            builder.setContentText(message);
            builder.setAutoCancel(true);
            builder.setVisibility(NotificationCompat.VISIBILITY_PRIVATE);

            // Adding actions
            Intent confirmIntent = new Intent(Utils.INTENT_ACTION_NOTIFICATION_CONFIRM);
            confirmIntent.putExtras(extras);

            PendingIntent confirmPendingIntent = PendingIntent.getBroadcast(context, 0, confirmIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            builder.addAction(new android.support.v4.app.NotificationCompat.Action(
                            R.drawable.ic_action_accept,
                            context.getString(R.string.action_confirm),
                            confirmPendingIntent)
            );

            Intent cancelIntent = new Intent(Utils.INTENT_ACTION_NOTIFICATION_CANCEL);
            cancelIntent.putExtra(Utils.INTENT_EXTRA_ROW_ID, rowID);

            PendingIntent cancelPendingIntent = PendingIntent.getBroadcast(context, 0, cancelIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            builder.addAction(new android.support.v4.app.NotificationCompat.Action(
                            R.drawable.ic_action_remove,
                            context.getString(R.string.action_cancel),
                            cancelPendingIntent)
            );

            // Sending notification
            builder.setContentIntent(contentIntent);
            notificationManager.notify(
                    recordType == DBContract.RECORD_INCOME ? Utils.NOTIFICATION_ID_INCOME : Utils.NOTIFICATION_ID_EXPENSE,
                    builder.build()
            );
        }
    }
}
