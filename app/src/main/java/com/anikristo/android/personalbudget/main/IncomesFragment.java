package com.anikristo.android.personalbudget.main;
/**
 * Author: Ani Kristo
 * Version: 1.0
 * Date: 22-Feb-2015
 * <p/>
 * Description: TODO
 */

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.anikristo.android.personalbudget.R;
import com.anikristo.android.personalbudget.database.DBContract;
import com.anikristo.android.personalbudget.database.DBHelper;
import com.anikristo.android.personalbudget.dialogs.RecordLongClickDialog;
import com.anikristo.android.personalbudget.dialogs.ViewRecordDialog;
import com.anikristo.android.personalbudget.misc.Category;
import com.anikristo.android.personalbudget.misc.CategoryHolder;
import com.anikristo.android.personalbudget.misc.Currency;
import com.anikristo.android.personalbudget.misc.CurrencyHolder;
import com.anikristo.android.personalbudget.misc.Utils;
import com.anikristo.android.personalbudget.records.RecordActivity;

import java.text.DecimalFormat;


public class IncomesFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    // other
    private static DBHelper dbHelper;

    // WIDGETS
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView emptyView;
    private FloatingActionButton fab;

    // Helper properties to undo row updates
    private Double oldAmount;
    private Currency oldCurrency;
    private Category oldCategory;
    private Integer oldPeriod;
    private Integer oldDay;
    private Integer oldMonth;
    private Integer oldYear;
    private Integer oldHour;
    private Integer oldMinute;
    private String oldComments;
    private Integer oldInsertionType;

    // METHODS
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_main_incomes, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.incomes_listView);
        emptyView = (TextView) view.findViewById(R.id.incomes_no_records_tv);
        fab = (FloatingActionButton) view.findViewById(R.id.incomes_fab);

        fab.setBackgroundTintList(getResources().getColorStateList(R.color.fab_incomes_selector));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), RecordActivity.class)
                        .putExtra(Utils.INTENT_EXTRA_RECORD_TYPE, DBContract.RECORD_INCOME)
                        .putExtra(Utils.INTENT_EXTRA_FUNCTION, RecordActivity.FUNCTION_ADD));
            }
        });

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.incomes_swipeToRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.accent_incomes, R.color.accent_expenses);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        // Swipe to delete ability
        ItemTouchHelper swipeToDismissTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
                deleteRecord((IncomesViewHolder) viewHolder);
            }
        });
        swipeToDismissTouchHelper.attachToRecyclerView(recyclerView);

        dbHelper = new DBHelper(getActivity());

        onRefresh();
        return view;
    }

    private void deleteRecord(IncomesViewHolder viewHolder) {
        long rowID = viewHolder.getRowID();
        final int position = viewHolder.getAdapterPosition();

        // Assign old values to be able to undo the remove operation
        new QueryRecordTask().execute(rowID, (long) position);

        // Displaying the Snackbar
        Snackbar.make(fab, R.string.info_record_deleted, Snackbar.LENGTH_LONG)
                .setAction(R.string.action_undo, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        undoDelete(position);
                    }
                }).show();
    }

    @Override
    public void onRefresh() {
        new PopulatingTask().execute();
    }

    public void itemAdded() {
        new PopulatingTask().execute(RecordActivity.FUNCTION_ADD, 0);
    }

    public void itemUpdated(int pos) {
        new PopulatingTask().execute(RecordActivity.FUNCTION_EDIT, pos);
    }

    public void undoUpdate(long rowID, int position) {
        dbHelper.updateIncomeRecord(rowID, oldAmount, oldCurrency, oldCategory, oldPeriod, oldDay, oldMonth,
                oldYear, oldHour, oldMinute, oldComments);
        new PopulatingTask().execute(RecordActivity.FUNCTION_EDIT, position);
    }

    private void undoDelete(int position) {
        dbHelper.insertIncomeRecord(oldInsertionType, oldAmount, oldCurrency, oldCategory, oldPeriod, oldDay, oldMonth, oldYear,
                oldHour, oldMinute, oldComments);
        new PopulatingTask().execute(RecordActivity.FUNCTION_ADD, position);
    }

    public View getFAB() {
        return fab;
    }

    /**
     * TODO
     */

    private class IncomesViewHolder extends RecyclerView.ViewHolder implements RecordLongClickDialog.RecordLongClickDialogListener {

        // PROPERTIES
        private final TextView amountTV;
        private final TextView categoryTV;
        private final TextView commentTV;
        private final ImageView periodIV;

        private Long rowID;

        // CONSTRUCTOR
        public IncomesViewHolder(View itemView) {
            super(itemView);

            amountTV = (TextView) itemView.findViewById(R.id.income_record_amount_tv);
            categoryTV = (TextView) itemView.findViewById(R.id.income_record_category_tv);
            commentTV = (TextView) itemView.findViewById(R.id.income_record_comment_tv);
            periodIV = (ImageView) itemView.findViewById(R.id.income_record_periodical_iv);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Cursor cursor = ((IncomeRecordAdapter) recyclerView.getAdapter()).getCursor();
                    cursor.moveToPosition(getAdapterPosition());

                    double amount = cursor.getDouble(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_AMOUNT));
                    String amountStr = new DecimalFormat("0.###").format(amount);
                    Currency currency = CurrencyHolder.getCurrency(cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_CURRENCY)));
                    Integer category = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_CATEGORY));
                    Integer period = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_PERIOD));
                    Integer day = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_DAY));
                    Integer month = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_MONTH));
                    Integer year = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_YEAR));
                    Integer hour = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_HOUR));
                    Integer minute = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_MINUTE));
                    String comments = cursor.getString(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_NOTES));
                    Integer insertionType = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_INSERTION_TYPE));

                    ViewRecordDialog dialog = new ViewRecordDialog();
                    dialog.setAmount(amountStr);
                    dialog.setCurrency(currency);
                    dialog.setCategory(CategoryHolder.getCategory(category, DBContract.RECORD_INCOME));
                    dialog.setPeriod(period);
                    dialog.setDate(day, month, year);
                    dialog.setTime(hour, minute);
                    dialog.setComments(comments);
                    dialog.setInsertiontype(insertionType);

                    dialog.setRecordType(DBContract.RECORD_INCOME);
                    dialog.show(getFragmentManager(), "RECORD_VIEW_DIALOG");
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {

                    RecordLongClickDialog dialog = new RecordLongClickDialog();
                    dialog.setListener(IncomesViewHolder.this);
                    dialog.show(getFragmentManager(), "LONG_CLICK_DIALOG");

                    return true;
                }
            });
        }

        // METHODS
        void setAmount(String amount) {
            amountTV.setText(amount);
        }

        void setCategory(Integer category) {
            categoryTV.setText(CategoryHolder.getCategory(category, DBContract.RECORD_INCOME).toString());
        }

        void setComment(String comment) {
            commentTV.setText(comment);
        }

        void setPeriod(boolean isPeriodical) {
            periodIV.setVisibility(isPeriodical ? View.VISIBLE : View.INVISIBLE);
        }

        long getRowID() {
            return rowID != null ? rowID : -1;
        }

        void setRowID(long id) {
            rowID = id;
        }

        @Override
        public void onSelectOption(int option) {
            if (option == RecordLongClickDialog.OPTION_EDIT) {
                // Preparing initiating intent
                Intent intent = new Intent(getActivity(), RecordActivity.class);

                // Obtaining the info from the cursor
                Cursor cursor = ((IncomeRecordAdapter) recyclerView.getAdapter()).getCursor();
                cursor.moveToPosition(getAdapterPosition());

                double amount = cursor.getDouble(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_AMOUNT));
                int currency = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_CURRENCY));
                Integer category = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_CATEGORY));
                Integer period = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_PERIOD));
                Integer day = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_DAY));
                Integer month = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_MONTH));
                Integer year = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_YEAR));
                Integer hour = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_HOUR));
                Integer minute = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_MINUTE));
                String comments = cursor.getString(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_NOTES));
                Integer insertionType = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_INSERTION_TYPE));

                // Assign these obtained values to the old fields to be able to UNDO the result
                oldAmount = amount;
                oldCurrency = CurrencyHolder.getCurrency(currency);
                oldCategory = CategoryHolder.getCategory(category, DBContract.RECORD_INCOME);
                oldPeriod = period;
                oldDay = day;
                oldMonth = month;
                oldYear = year;
                oldHour = hour;
                oldMinute = minute;
                oldComments = comments;
                oldInsertionType = insertionType;

                // Populating with the necessary data
                intent.putExtra(Utils.INTENT_EXTRA_ROW_ID, cursor.getLong(cursor.getColumnIndexOrThrow(DBContract.RecordTable._ID)));
                intent.putExtra(Utils.INTENT_EXTRA_AMOUNT, amount);
                intent.putExtra(Utils.INTENT_EXTRA_CURRENCY, currency);
                intent.putExtra(Utils.INTENT_EXTRA_CATEGORY, category);
                intent.putExtra(Utils.INTENT_EXTRA_PERIOD, period);
                intent.putExtra(Utils.INTENT_EXTRA_DAY, day);
                intent.putExtra(Utils.INTENT_EXTRA_MONTH, month);
                intent.putExtra(Utils.INTENT_EXTRA_YEAR, year);
                intent.putExtra(Utils.INTENT_EXTRA_HOUR, hour);
                intent.putExtra(Utils.INTENT_EXTRA_MINUTE, minute);
                intent.putExtra(Utils.INTENT_EXTRA_NOTES, comments);
                intent.putExtra(Utils.INTENT_EXTRA_INSERTION_TYPE, insertionType);
                intent.putExtra(Utils.INTENT_EXTRA_ADAPTER_POS, getAdapterPosition());

                intent.putExtra(Utils.INTENT_EXTRA_RECORD_TYPE, DBContract.RECORD_INCOME);
                intent.putExtra(Utils.INTENT_EXTRA_FUNCTION, RecordActivity.FUNCTION_EDIT);

                // Starting Activity
                startActivity(intent);

            } else if (option == RecordLongClickDialog.OPTION_DELETE) {
                deleteRecord(IncomesViewHolder.this);
            }
        }
    }

    /**
     * TODO
     */
    private class IncomeRecordAdapter extends RecyclerView.Adapter<IncomesViewHolder> {

        // PROPERTIES
        private final LayoutInflater inflater;
        private Cursor cursor;

        // CONSTRUCTOR
        public IncomeRecordAdapter(Context context, Cursor c) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            cursor = c;
        }

        // METHODS
        @Override
        public IncomesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = inflater.inflate(R.layout.item_record_income, parent, false);
            return new IncomesViewHolder(view);
        }

        @Override
        public int getItemCount() {
            return cursor.getCount();
        }

        @Override
        public void onBindViewHolder(IncomesViewHolder holder, int position) {

            cursor.moveToPosition(position);

            // Filling the 'Amount' field

            // Modifying the sum view to shorten the length
            // example: 1200$ = 1.2K $ or 12500000$ = 1.25M $
            double sum = cursor.getDouble(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_AMOUNT));
            String sumStr = Utils.formatSum(sum, 1);

            // Determining the currency and adding currency symbol
            int currencyPos = cursor.getInt(cursor.getColumnIndex(DBContract.RecordTable.KEY_CURRENCY));
            String currencyStr = CurrencyHolder.getCurrency(currencyPos).getSymbol();
            String amountStr = sumStr + " " + currencyStr;

            // Filling the 'Comment' field

            // When there are not comments, the 'Comment' field will have the date and time added
            String commentStr;
            if (cursor.isNull(cursor.getColumnIndex(DBContract.RecordTable.KEY_NOTES))) {

                Integer day = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_DAY));
                Integer month = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_MONTH));
                Integer year = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_YEAR));
                Integer hour = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_HOUR));
                Integer minute = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_MINUTE));

                String dateStr = Utils.formatDate(day, month, year);
                String timeStr = Utils.formatTime(hour, minute);
                commentStr = "Added on: " + dateStr + " " + timeStr; // todo

            } else {
                commentStr = cursor.getString(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_NOTES));
                if (commentStr.length() > 40)
                    commentStr = commentStr.substring(0, 37) + "...";
            }

            // Setting category
            Integer category = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_CATEGORY));

            // Determining if is periodical or not
            Integer period = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_PERIOD));

            // Setting the row id
            holder.setRowID(cursor.getLong(cursor.getColumnIndexOrThrow(DBContract.RecordTable._ID)));

            // Setting the other settings
            holder.setAmount(amountStr);
            holder.setCategory(category);
            holder.setComment(commentStr);
            holder.setPeriod(period != 0);
        }

        Cursor getCursor() {
            return cursor;
        }

        void changeCursor(Cursor c) {
            cursor = c;
        }
    }

    /**
     * TODO
     */
    private class PopulatingTask extends AsyncTask<Integer, Void, Cursor> {

        // PROPERTIES
        Integer taskType;
        Integer itemPosition;

        // METHODS
        @Override
        protected Cursor doInBackground(Integer... params) {

            if (params.length >= 2) {
                taskType = params[0];
                itemPosition = params[1];
            }

            return dbHelper.getAllIncomesDesc();
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            super.onPostExecute(cursor);

            // Get adapter reference to update it
            IncomeRecordAdapter adapter = (IncomeRecordAdapter) recyclerView.getAdapter();

            // Update the adapter with the new cursor
            if (adapter != null) {
                adapter.changeCursor(cursor);
            } else {
                adapter = new IncomeRecordAdapter(getActivity(), cursor);
                recyclerView.setAdapter(adapter);
            }

            // Determine if the list is empty
            if (adapter.getItemCount() == 0) {
                emptyView.setVisibility(View.VISIBLE);
            } else {
                emptyView.setVisibility(View.GONE);
            }

            // Notifying for the changes
            if (taskType != null) { // if taskType is null then this is called from OnRefresh()
                if (taskType == RecordActivity.FUNCTION_ADD) {
                    adapter.notifyItemInserted(0);
                    recyclerView.scrollToPosition(0);
                } else if (taskType == RecordActivity.FUNCTION_EDIT) {
                    adapter.notifyItemChanged(itemPosition);
                } else if (taskType == RecordActivity.FUNCTION_REMOVE) {
                    adapter.notifyItemRemoved(itemPosition);
                }
            } else {
                adapter.notifyDataSetChanged();
            }

            // Stop refreshing
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    class QueryRecordTask extends AsyncTask<Long, Void, Cursor> {

        // PROPERTIES
        private Long rowID;
        private Integer position;

        // METHODS
        @Override
        protected Cursor doInBackground(Long... params) {
            if (params.length >= 2) {
                rowID = params[0];
                position = params[1].intValue();
            }

            return dbHelper.getIncomeRecord(rowID);
        }

        @Override
        protected void onPostExecute(Cursor c) {
            super.onPostExecute(c);

            oldAmount = c.getDouble(c.getColumnIndexOrThrow(DBContract.RecordTable.KEY_AMOUNT));
            oldCurrency = CurrencyHolder.getCurrency(c.getInt(c.getColumnIndexOrThrow(DBContract.RecordTable.KEY_CURRENCY)));
            oldCategory = CategoryHolder.getCategory(c.getInt(c.getColumnIndexOrThrow(DBContract.RecordTable.KEY_CATEGORY)), DBContract.RECORD_INCOME);
            oldPeriod = c.getInt(c.getColumnIndexOrThrow(DBContract.RecordTable.KEY_PERIOD));
            oldDay = c.getInt(c.getColumnIndexOrThrow(DBContract.RecordTable.KEY_DAY));
            oldMonth = c.getInt(c.getColumnIndexOrThrow(DBContract.RecordTable.KEY_MONTH));
            oldYear = c.getInt(c.getColumnIndexOrThrow(DBContract.RecordTable.KEY_YEAR));
            oldHour = c.getInt(c.getColumnIndexOrThrow(DBContract.RecordTable.KEY_HOUR));
            oldMinute = c.getInt(c.getColumnIndexOrThrow(DBContract.RecordTable.KEY_MINUTE));
            oldComments = c.getString(c.getColumnIndexOrThrow(DBContract.RecordTable.KEY_NOTES));
            oldInsertionType = c.getInt(c.getColumnIndexOrThrow(DBContract.RecordTable.KEY_INSERTION_TYPE));

            // Delete the record
            dbHelper.deleteIncomeRecord(rowID);

            // Notify about the removal
            new PopulatingTask().execute(RecordActivity.FUNCTION_REMOVE, position);
        }
    }
}
