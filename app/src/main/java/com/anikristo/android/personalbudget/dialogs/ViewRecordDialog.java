package com.anikristo.android.personalbudget.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anikristo.android.personalbudget.R;
import com.anikristo.android.personalbudget.database.DBContract;
import com.anikristo.android.personalbudget.misc.Category;
import com.anikristo.android.personalbudget.misc.Currency;
import com.anikristo.android.personalbudget.misc.Utils;

public class ViewRecordDialog extends DialogFragment {

    // PROPERTIES
    private String amount;
    private Currency currency;
    private String comments;
    private Integer period;
    private String date;
    private String time;
    private Category category;
    private int recordType;
    private int insertionType;

    // METHODS
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // Creating the view
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_dialog_record_view, null);

        // Find the references to the the widgets
        TextView amountTV = (TextView) view.findViewById(R.id.recordViewDialog_amount_tv);
        TextView categoryTV = (TextView) view.findViewById(R.id.recordViewDialog_category_tv);
        TextView commentTV = (TextView) view.findViewById(R.id.recordViewDialog_comment_tv);
        TextView dateTV = (TextView) view.findViewById(R.id.recordViewDialog_date_tv);
        TextView frequencyTV = (TextView) view.findViewById(R.id.recordViewDialog_frequency_tv);
        LinearLayout periodLayout = (LinearLayout) view.findViewById(R.id.recordViewDialog_period_layout);

        // Fill out the widgets or make them invisible if not applicable
        amountTV.setText(amount + " " + currency.getFullName());
        dateTV.setText(date + " - " + time);

        if (period == null || period == 0)
            periodLayout.setVisibility(View.GONE);
        else {
            String day = getString(R.string.day);
            String days = getString(R.string.days);
            String str = String.format(getString(R.string.info_recordViewDialog_frequency), period, period == 1 ? day : days);
            if (insertionType == DBContract.INSERTION_TYPE_AUTO)
                str += "\n" + getString(R.string.auto);
            frequencyTV.setText(str);
        }

        if (comments == null)
            commentTV.setVisibility(View.GONE);
        else
            commentTV.setText(comments);

        if (category == null)
            categoryTV.setVisibility(View.GONE);
        else
            categoryTV.setText(category.toString());

        if (recordType == DBContract.RECORD_INCOME) {
            amountTV.setTextColor(getResources().getColor(R.color.accent_incomes_dark));
        } else if (recordType == DBContract.RECORD_EXPENSE) {
            amountTV.setTextColor(getResources().getColor(R.color.accent_expenses_dark));
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);

        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public void setComments(String comments) {
        if (comments != null)
            this.comments = "\"" + comments + "\"";
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public void setDate(Integer day, Integer month, Integer year) {
        this.date = Utils.formatDate(day, month, year);
    }

    public void setTime(Integer hour, Integer minute) {
        this.time = Utils.formatTime(hour, minute);
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setRecordType(int recordType) {
        this.recordType = recordType;
    }

    public void setInsertiontype(int insertionType) {
        this.insertionType = insertionType;
    }

}
