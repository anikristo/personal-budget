package com.anikristo.android.personalbudget.misc;

import java.util.Arrays;
import java.util.List;

/**
 * Author: Ani Kristo
 * Version: 1.0
 * Date: 22-Feb-2015
 * <p/>
 * Description: This enumerator provides the inherent currency types for the app and the useful
 * methods for accessing information about the elements.
 */
public class Currency {

    // PROPERTIES
    private final String symbol;
    private final String abbr;
    private final String fullName;
    private final String[] countryISO2;

    // CONSTRUCTOR
    public Currency(String symbol, String abbr, String fullName, String... countryISO2) {
        this.symbol = symbol;
        this.abbr = abbr;
        this.fullName = fullName;
        this.countryISO2 = countryISO2;
    }

    // METHODS
    public String getSymbol() {
        return symbol;
    }

    public String getAbbreviation() {
        return abbr;
    }

    public String getFullName() {
        return fullName;
    }

    public List<String> getCountryISO2List() {
        return Arrays.asList(countryISO2);
    }

    public boolean hasCountryISO(String iso) {
        for (String s : countryISO2) {
            if (s.equals(iso))
                return true;
        }

        return false;
    }


}
