package com.anikristo.android.personalbudget.main;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.anikristo.android.personalbudget.R;
import com.anikristo.android.personalbudget.database.DBContract;
import com.anikristo.android.personalbudget.database.DBHelper;
import com.anikristo.android.personalbudget.dialogs.ViewRecordDialog;
import com.anikristo.android.personalbudget.misc.CurrencyHolder;
import com.anikristo.android.personalbudget.misc.Utils;
import com.anikristo.android.personalbudget.records.RecordActivity;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HomeFragment extends Fragment {

    // CONSTANTS
    public static final int RECENT_RECORDS_LIMIT = 3;

    // Other
    private static DBHelper dbHelper;
    private double totalIncomes;
    private double totalExpenses;

    // WIDGETS
    private TextView totalIncomesTV;
    private TextView totalExpensesTV;
    private RecyclerView recentList;
    private FloatingActionsMenu FABMenu;
    private PieChart chart;

    // METHODS
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_main_home, container, false);
        totalIncomesTV = (TextView) v.findViewById(R.id.home_total_incomes_tv);
        totalExpensesTV = (TextView) v.findViewById(R.id.home_total_expenses_tv);
        recentList = (RecyclerView) v.findViewById(R.id.home_recent_list);
        FloatingActionButton addIncomesFAB = (FloatingActionButton) v.findViewById(R.id.home_fab_add_incomes);
        FloatingActionButton addExpensesFAB = (FloatingActionButton) v.findViewById(R.id.home_fab_add_expenses);
        FABMenu = (FloatingActionsMenu) v.findViewById(R.id.home_fab_menu);
        chart = (PieChart) v.findViewById(R.id.home_chart);
        chart.setDescription(null);

        addIncomesFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), RecordActivity.class)
                        .putExtra(Utils.INTENT_EXTRA_RECORD_TYPE, DBContract.RECORD_INCOME)
                        .putExtra(Utils.INTENT_EXTRA_FUNCTION, RecordActivity.FUNCTION_ADD));
                ((MainActivity) getActivity()).showIncomes();
                FABMenu.collapse();
            }
        });

        addExpensesFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), RecordActivity.class)
                        .putExtra(Utils.INTENT_EXTRA_RECORD_TYPE, DBContract.RECORD_EXPENSE)
                        .putExtra(Utils.INTENT_EXTRA_FUNCTION, RecordActivity.FUNCTION_ADD));
                ((MainActivity) getActivity()).showExpenses();
                FABMenu.collapse();
            }
        });

        recentList.setLayoutManager(new LinearLayoutManager(getActivity()));

        dbHelper = new DBHelper(getActivity());
        return v;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (isAdded()) {
            refresh();
        }
    }

    public void refresh() {
//        new TotalsTask().execute();
//        new RecentRecordsTask().execute();
        new ChartSetupTask().execute();
    }

    // Struct
    private class RecentRecord {
        int recordType;
        double amount;
        int currency;
        int period;
        int day;
        int month;
        int year;
        int hour;
        int minute;
        String comments;
    }

    private class TotalsTask extends AsyncTask<Void, Void, List<Integer>> {

        @Override
        protected List<Integer> doInBackground(Void... params) {
            Cursor incomes = dbHelper.getAllIncomesDesc();
            Cursor expenses = dbHelper.getAllExpensesDesc();

            int sumIncomes = 0;
            int sumExpenses = 0;

            while (!incomes.isAfterLast()) {
                sumIncomes += incomes.getInt(incomes.getColumnIndexOrThrow(DBContract.RecordTable.KEY_AMOUNT));
                incomes.moveToNext();
            }

            while (!expenses.isAfterLast()) {
                sumExpenses += expenses.getInt(expenses.getColumnIndexOrThrow(DBContract.RecordTable.KEY_AMOUNT));
                expenses.moveToNext();
            }

            return Arrays.asList(sumIncomes, sumExpenses);
        }

        @Override
        protected void onPostExecute(List<Integer> result) {
            super.onPostExecute(result);

            int incomesSum = result.get(0);
            int expensesSum = result.get(1);

            totalIncomesTV.setText(Utils.formatSum(incomesSum, 2));
            totalExpensesTV.setText(Utils.formatSum(expensesSum, 2));
        }
    }

    private class RecentRecordsTask extends AsyncTask<Void, Void, List<RecentRecord>> {

        @Override
        protected List<RecentRecord> doInBackground(Void... params) {
            List<RecentRecord> list = new ArrayList<>(5);

            for (Cursor cursor = dbHelper.getRecentRecords(); !cursor.isAfterLast(); cursor.moveToNext()) {
                RecentRecord recent = new RecentRecord();
                recent.amount = cursor.getDouble(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_AMOUNT));
                recent.currency = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_CURRENCY));
                recent.period = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_PERIOD));
                recent.recordType = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_RECORD_TYPE));
                recent.day = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_DAY));
                recent.month = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_MONTH));
                recent.year = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_YEAR));
                recent.hour = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_HOUR));
                recent.minute = cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_MINUTE));
                recent.comments = cursor.getString(cursor.getColumnIndexOrThrow(DBContract.RecordTable.KEY_NOTES));

                list.add(recent);
            }
            return list;
        }

        @Override
        protected void onPostExecute(List<RecentRecord> recentRecords) {
            super.onPostExecute(recentRecords);

            // Get adapter reference to update it
            RecentRecordAdapter adapter = (RecentRecordAdapter) recentList.getAdapter();

            // Update the adapter with the new list
            if (adapter != null) {
                adapter.changeList(recentRecords);
            } else {
                adapter = new RecentRecordAdapter(getActivity(), recentRecords);
                recentList.setAdapter(adapter);
            }
        }
    }

    private class RecentRecordVH extends RecyclerView.ViewHolder {

        // PROPERTIES
        final ImageView recordTypeIV;
        final TextView amountTV;
        final TextView dateTV;
        final ImageView periodIV;

        // CONSTRUCTOR
        public RecentRecordVH(View itemView) {
            super(itemView);

            recordTypeIV = (ImageView) itemView.findViewById(R.id.home_recentRecord_recordType_iv);
            amountTV = (TextView) itemView.findViewById(R.id.home_recent_record_amount_tv);
            dateTV = (TextView) itemView.findViewById(R.id.home_recent_record_date_tv);
            periodIV = (ImageView) itemView.findViewById(R.id.home_recent_record_periodical_iv);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    List<RecentRecord> list = ((RecentRecordAdapter) recentList.getAdapter()).getList();
                    RecentRecord record = list.get(getAdapterPosition());

                    ViewRecordDialog dialog = new ViewRecordDialog();
                    dialog.setAmount(new DecimalFormat("0.###").format(record.amount));
                    dialog.setCurrency(CurrencyHolder.getCurrency(record.currency));
                    dialog.setPeriod(record.period);
                    dialog.setDate(record.day, record.month, record.year);
                    dialog.setTime(record.hour, record.minute);
                    dialog.setComments(record.comments);
                    dialog.setRecordType(record.recordType);
                    dialog.show(getFragmentManager(), "RECORD_VIEW_DIALOG");
                }
            });
        }

        // METHODS
        void setRecordType(int recordType) {
            if (recordType == DBContract.RECORD_INCOME) {
                recordTypeIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_incomes));
            } else if (recordType == DBContract.RECORD_EXPENSE) {
                recordTypeIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_expenses));
            }
        }

        void setAmountStr(String amount) {
            amountTV.setText(amount);
        }

        void setDate(String date) {
            dateTV.setText(date);
        }

        void setPeriodical(boolean isPeriodical) {
            periodIV.setVisibility(isPeriodical ? View.VISIBLE : View.GONE);
        }
    }

    private class RecentRecordAdapter extends RecyclerView.Adapter<RecentRecordVH> {

        // PROPERTIES
        private final LayoutInflater inflater;
        private List<RecentRecord> list;

        // CONSTRUCTOR
        public RecentRecordAdapter(Context context, List<RecentRecord> list) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.list = list;
        }

        // METHODS
        @Override
        public RecentRecordVH onCreateViewHolder(ViewGroup parent, int viewType) {
            return new RecentRecordVH(inflater.inflate(R.layout.item_home_recent_record, parent, false));
        }

        @Override
        public void onBindViewHolder(RecentRecordVH holder, int position) {
            RecentRecord record = list.get(position);

            // Filling the 'Amount' field
            String sumStr = Utils.formatSum(record.amount, 1);

            // Determining the currency and adding currency symbol
            int currencyPos = record.currency;
            String currencyStr = CurrencyHolder.getCurrency(currencyPos).getSymbol();
            String amountStr = sumStr + " " + currencyStr;

            // Filling the 'Date' field
            String dateStr = "Added on: "; // todo
            dateStr += Utils.formatDate(record.day, record.month, record.year);

            // Setting the other settings
            holder.setRecordType(record.recordType);
            holder.setAmountStr(amountStr);
            holder.setDate(dateStr);
            holder.setPeriodical(record.period > 0);
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        List<RecentRecord> getList() {
            return list;
        }

        void changeList(List<RecentRecord> list) {
            this.list = list;
        }
    }

    private class ChartSetupTask extends AsyncTask<Void, Void, Object[]> {

        @SuppressWarnings("ConstantConditions")
        @Override
        protected Object[] doInBackground(Void... params) {
            double incSum = dbHelper.getTotalIncomes();
            double expSum = dbHelper.getTotalExpenses();

            ArrayList<Entry> entries = new ArrayList<>();
            ArrayList<String> xVals = new ArrayList<>();

            xVals.add(getString(R.string.expenses));
            entries.add(new Entry((float) expSum, 0));

            totalIncomes = incSum;
            totalExpenses = expSum;

            if (incSum >= expSum) {
                xVals.add(getString(R.string.savings));
                entries.add(new Entry((float) (incSum - expSum), 1));
            }

            String percentage = null;
            if (incSum > 0) {
                percentage = new DecimalFormat("0.##").format(expSum / incSum * 100);
            }

            return new Object[]{entries, xVals, percentage};
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void onPostExecute(Object[] list) {
            super.onPostExecute(list);

            ArrayList<Entry> entries = (ArrayList<Entry>) list[0];
            ArrayList<String> xVals = (ArrayList<String>) list[1];
            String percentage = (String) list[2];

            PieDataSet dataSet = new PieDataSet(entries, getString(R.string.title_expenses));
            dataSet.setSliceSpace(2);
            dataSet.setValueTextColor(getResources().getColor(android.R.color.white));
            dataSet.setColors(Utils.getChartColorList(2, true), getActivity());

            PieData data = new PieData(xVals, dataSet);
            chart.setData(data);

            if (percentage != null)
                chart.setCenterText(getString(R.string.expenditures) + percentage + " %");// TODO
            else
                chart.setCenterText(getString(R.string.no_incomes));
            chart.getLegend().setEnabled(false);

            chart.animateXY(1000, 1000);
        }
    }
}