package com.anikristo.android.personalbudget.misc.interfaces;

public interface IMoreFragment extends IBaseFragment {
    int PERIOD = 3;
    int DATE = 4;
    int TIME = 5;
    int NOTES = 6;

    String getNotes();

    void setNotes(String comments);

    Integer getPeriod();

    void setPeriod(int period);

    Integer getDay();

    Integer getMonth();

    Integer getYear();

    void setDate(Integer day, Integer month, Integer year);

    Integer getHour();

    Integer getMinute();

    void setTime(Integer hour, Integer minute);
}
