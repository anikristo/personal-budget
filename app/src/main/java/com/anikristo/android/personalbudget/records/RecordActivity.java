package com.anikristo.android.personalbudget.records;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.anikristo.android.personalbudget.R;
import com.anikristo.android.personalbudget.database.DBContract;
import com.anikristo.android.personalbudget.database.DBHelper;
import com.anikristo.android.personalbudget.dialogs.CurrencyPickerDialog;
import com.anikristo.android.personalbudget.misc.Category;
import com.anikristo.android.personalbudget.misc.Currency;
import com.anikristo.android.personalbudget.misc.CurrencyHolder;
import com.anikristo.android.personalbudget.misc.Utils;

public class RecordActivity extends AppCompatActivity implements CurrencyPickerDialog.CurrencyDialogInterface {

    // CONSTANTS
    public static final int FUNCTION_ADD = 0x20;
    public static final int FUNCTION_EDIT = 0x21;
    public static final int FUNCTION_REMOVE = 0x22;

    public static final int STATUS_INSERTED = 0x11;
    public static final int STATUS_UNSUCCESSFUL = 0x12;
    public static final int STATUS_NAV_UP = 0x13;
    public static final int STATUS_UPDATED = 0x14;

    // Database Helper
    private static DBHelper dbHelper;

    // Other
    private int recordType;
    private int function;
    private Intent closingIntent;

    private FloatingActionButton fab;

    // Fragments
    private MainFragment mainFragment;
    private MoreFragment moreFragment;

    // METHODS
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);

        // Toolbars
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_record);
        setSupportActionBar(toolbar);

        // FAB
        fab = (FloatingActionButton) findViewById(R.id.record_fab);
        fab.requestFocus();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // PROPERTIES
                double amount = mainFragment.getAmount();
                Currency currency = mainFragment.getCurrency();
                Integer period = moreFragment.getPeriod();
                Category category = mainFragment.getCategory();
                Integer day = moreFragment.getDay();
                Integer month = moreFragment.getMonth();
                Integer year = moreFragment.getYear();
                Integer hour = moreFragment.getHour();
                Integer minute = moreFragment.getMinute();
                String notes = moreFragment.getNotes();

                // Preparing closingIntent to be broadcast
                closingIntent = new Intent(Utils.INTENT_ACTION_RECORD_RESULT);
                closingIntent.putExtra(Utils.INTENT_EXTRA_INSERTION_TYPE, DBContract.INSERTION_TYPE_MANUAL);
                if (mainFragment.contains(MainFragment.AMOUNT))
                    closingIntent.putExtra(Utils.INTENT_EXTRA_AMOUNT, amount);

                if (mainFragment.contains(MainFragment.CURRENCY))
                    closingIntent.putExtra(Utils.INTENT_EXTRA_CURRENCY, CurrencyHolder.getPosition(currency));

                if (moreFragment.contains(MoreFragment.PERIOD))
                    closingIntent.putExtra(Utils.INTENT_EXTRA_PERIOD, period);

                if (moreFragment.contains(MoreFragment.DATE)) {
                    closingIntent.putExtra(Utils.INTENT_EXTRA_DAY, day);
                    closingIntent.putExtra(Utils.INTENT_EXTRA_MONTH, month);
                    closingIntent.putExtra(Utils.INTENT_EXTRA_YEAR, year);
                }

                if (moreFragment.contains(MoreFragment.TIME)) {
                    closingIntent.putExtra(Utils.INTENT_EXTRA_HOUR, hour);
                    closingIntent.putExtra(Utils.INTENT_EXTRA_MINUTE, minute);
                }

                if (moreFragment.contains(MoreFragment.NOTES))
                    closingIntent.putExtra(Utils.INTENT_EXTRA_NOTES, notes);

                if (mainFragment.contains((MainFragment.CATEGORY)))
                    closingIntent.putExtra(Utils.INTENT_EXTRA_CATEGORY, category.getID());

                closingIntent.putExtra(Utils.INTENT_EXTRA_RECORD_TYPE, recordType);

                // UPDATE button
                if (function == FUNCTION_EDIT) {

                    // Check for empty amount field
                    if (amount == 0) {
                        mainFragment.showAmountErrorTV(true);
                    }

                    // Check if the switch is on and period is valid
                    else if (moreFragment.isPeriodSwitchOn() && period == null) {
                        showDialogEnterPeriod();

                    } else { // No problem. Can update now.

                        final long rowID = getIntent().getLongExtra(Utils.INTENT_EXTRA_ROW_ID, -1);
                        boolean result = false;
                        if (recordType == DBContract.RECORD_INCOME)
                            result = dbHelper.updateIncomeRecord(
                                    rowID,
                                    amount,
                                    currency,
                                    category,
                                    period,
                                    day,
                                    month,
                                    year,
                                    hour,
                                    minute,
                                    notes
                            );

                        else if (recordType == DBContract.RECORD_EXPENSE)
                            result = dbHelper.updateExpenseRecord(
                                    rowID,
                                    amount,
                                    currency,
                                    category,
                                    period,
                                    day,
                                    month,
                                    year,
                                    hour,
                                    minute,
                                    notes
                            );

                        closingIntent.putExtra(Utils.INTENT_EXTRA_FUNCTION, FUNCTION_EDIT);

                        if (result) {
                            closingIntent.putExtra(Utils.INTENT_EXTRA_STATUS, STATUS_UPDATED);
                            closingIntent.putExtra(Utils.INTENT_EXTRA_ROW_ID, rowID);
                            closingIntent.putExtra(Utils.INTENT_EXTRA_ADAPTER_POS, getIntent().getIntExtra(Utils.INTENT_EXTRA_ADAPTER_POS, -1));
                        } else {
                            closingIntent.putExtra(Utils.INTENT_EXTRA_STATUS, STATUS_UNSUCCESSFUL);
                        }

                        finish();
                    }
                } // Continues if add activity
                // end of if( action == update
                else if (function == FUNCTION_ADD) {
                    closingIntent.putExtra(Utils.INTENT_EXTRA_FUNCTION, FUNCTION_ADD);

                    // Check for empty amount field
                    if (amount == 0) {
                        mainFragment.showAmountErrorTV(true);
                    }
                    // Check if the switch is on and period is valid
                    else if (moreFragment.isPeriodSwitchOn() && period == null) {
                        showDialogEnterPeriod();

                    } else { // amount field is not empty
                        if (recordType == DBContract.RECORD_INCOME) {
                            long result = dbHelper.insertIncomeRecord(
                                    DBContract.INSERTION_TYPE_MANUAL,
                                    amount,
                                    currency,
                                    category,
                                    period,
                                    day,
                                    month,
                                    year,
                                    hour,
                                    minute,
                                    notes
                            );

                            if (result >= 0) {
                                // Sending an closingIntent to notify the main activity that a record
                                // has been added successfully
                                closingIntent.putExtra(Utils.INTENT_EXTRA_STATUS, STATUS_INSERTED);
                                closingIntent.putExtra(Utils.INTENT_EXTRA_ROW_ID, result);
                            } else {
                                // Sending an closingIntent to notify the main activity that a record
                                // has not been added successfully
                                closingIntent.putExtra(Utils.INTENT_EXTRA_STATUS, STATUS_UNSUCCESSFUL);
                            }
                        } else if (recordType == DBContract.RECORD_EXPENSE) {
                            long result = dbHelper.insertExpenseRecord(
                                    DBContract.INSERTION_TYPE_MANUAL,
                                    amount,
                                    currency,
                                    category,
                                    period,
                                    day,
                                    month,
                                    year,
                                    hour,
                                    minute,
                                    notes
                            );
                            if (result >= 0) {
                                // Sending an closingIntent to notify the main activity that a record
                                // has been added successfully
                                closingIntent.putExtra(Utils.INTENT_EXTRA_STATUS, STATUS_INSERTED);
                                closingIntent.putExtra(Utils.INTENT_EXTRA_ROW_ID, result);

                            } else {
                                // Sending an closingIntent to notify the main activity that a record
                                // has been added successfully
                                closingIntent.putExtra(Utils.INTENT_EXTRA_STATUS, STATUS_UNSUCCESSFUL);
                            }
                        }
                        finish(); // returns to the the selected section of the navigation
                    } // end of if (action == confirm)
                }
            }
        });

        // DB Helper
        dbHelper = new DBHelper(this);

        // Assigning the fragments
        mainFragment = new MainFragment();
        getFragmentManager().beginTransaction().replace(R.id.record_main_frame, mainFragment).commit();

        moreFragment = new MoreFragment();
        getFragmentManager().beginTransaction().replace(R.id.record_more_frame, moreFragment).commit();

        // Stylizing
        recordType = getIntent().getIntExtra(Utils.INTENT_EXTRA_RECORD_TYPE, DBContract.RECORD_INCOME);
        function = getIntent().getIntExtra(Utils.INTENT_EXTRA_FUNCTION, FUNCTION_ADD);
        stylizeAndPopulate(getIntent().getExtras());
    }

    private void showDialogEnterPeriod() {
        // New dialog to instruct the user to enter the period
        AlertDialog enterPeriodDialog = new AlertDialog.Builder(RecordActivity.this).create();
        enterPeriodDialog.setMessage(getString(R.string.dialog_warning_periodFieldEmpty));
        enterPeriodDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.got_it), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                moreFragment.focusPeriodField();
            }
        });
        enterPeriodDialog.setCanceledOnTouchOutside(true);
        enterPeriodDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // UP button
        if (item.getItemId() == R.id.home) {
            /* when Home button is pressed it redirects the user
            to the first section of the main activity */
            NavUtils.navigateUpFromSameTask(this);
            closingIntent = new Intent(Utils.INTENT_ACTION_RECORD_RESULT);
            closingIntent.putExtra(Utils.INTENT_EXTRA_STATUS, STATUS_NAV_UP);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dbHelper.close();

        if (closingIntent != null)
            LocalBroadcastManager.getInstance(this).sendBroadcast(closingIntent);
    }

    private void stylizeAndPopulate(Bundle extras) {

        // Populating the view with the extras
        if (extras.containsKey(Utils.INTENT_EXTRA_AMOUNT))
            mainFragment.setAmount(extras.getDouble(Utils.INTENT_EXTRA_AMOUNT));

        if (extras.containsKey(Utils.INTENT_EXTRA_CURRENCY))
            mainFragment.setCurrency(extras.getInt(Utils.INTENT_EXTRA_CURRENCY));

        if (extras.containsKey(Utils.INTENT_EXTRA_PERIOD))
            moreFragment.setPeriod(extras.getInt(Utils.INTENT_EXTRA_PERIOD));

        if (extras.containsKey(Utils.INTENT_EXTRA_DAY)
                && extras.containsKey(Utils.INTENT_EXTRA_MONTH)
                && extras.containsKey(Utils.INTENT_EXTRA_YEAR))

            moreFragment.setDate(
                    extras.getInt(Utils.INTENT_EXTRA_DAY),
                    extras.getInt(Utils.INTENT_EXTRA_MONTH),
                    extras.getInt(Utils.INTENT_EXTRA_YEAR)
            );

        if (extras.containsKey(Utils.INTENT_EXTRA_HOUR) && extras.containsKey(Utils.INTENT_EXTRA_MINUTE))
            moreFragment.setTime(extras.getInt(Utils.INTENT_EXTRA_HOUR), extras.getInt(Utils.INTENT_EXTRA_MINUTE));

        if (extras.containsKey(Utils.INTENT_EXTRA_NOTES))
            moreFragment
                    .setNotes(extras.getString(Utils.INTENT_EXTRA_NOTES));

        if (extras.containsKey(Utils.INTENT_EXTRA_CATEGORY))
            mainFragment.setCategory(extras.getInt(Utils.INTENT_EXTRA_CATEGORY));

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        if (function == FUNCTION_ADD) {

            if (recordType == DBContract.RECORD_EXPENSE) {
                setTitle(getString(R.string.title_expenses));
                fab.setBackgroundTintList(getResources().getColorStateList(R.color.fab_expenses_selector));
                setTheme(R.style.recordActivity_expenses);
            } else {
                setTitle(getString(R.string.title_incomes));
                fab.setBackgroundTintList(getResources().getColorStateList(R.color.fab_incomes_selector));
                setTheme(R.style.recordActivity_incomes);
            }

        } else if (function == FUNCTION_EDIT) {

            setTitle(R.string.title_activity_edit_record);

            if (recordType == DBContract.RECORD_EXPENSE) {
                fab.setBackgroundTintList(getResources().getColorStateList(R.color.fab_expenses_selector));
                setTheme(R.style.recordActivity_expenses);

            } else {
                fab.setBackgroundTintList(getResources().getColorStateList(R.color.fab_incomes_selector));
                setTheme(R.style.recordActivity_incomes);
            }
        }
    }

    @Override
    public void onCurrencyPicked(int position) {
        mainFragment.setCurrency(position);
    }
}