package com.anikristo.android.personalbudget.database;
/**
 * Author: Ani Kristo
 * Version: 1.0
 * Date: 22-Feb-2015
 * <p/>
 * Description: DBContract stands for Database Contract class. It is created in order to define
 * the attributes of the tables and the database itself and provide an interface for the
 * necessary SQL statements string etc.
 */

import android.provider.BaseColumns;

public final class DBContract {

    // CONSTANTS
    public static final String DB_NAME = "uB.db";
    public static final int DB_VERSION = 12;
    public static final int NUMBER_OF_TABLES = 1;

    public static final int RECORD_INCOME = 0x0A;
    public static final int RECORD_EXPENSE = 0x0B;

    public static final int INSERTION_TYPE_MANUAL = 0x1A;
    public static final int INSERTION_TYPE_AUTO = 0x1B;

    public static final int REPEATING_ON = 1;
    public static final int REPEATING_OFF = 0;

    public static final String[] CREATE_ALL_TABLES = {
            RecordTable.CREATE_TABLE};
    public static final String[] DROP_ALL_TABLES = {
            RecordTable.DELETE_TABLE};

    // Data Types
    private static final String RECORD_TYPE = " INTEGER";
    private static final String INSERTION_TYPE = " INTEGER";
    private static final String AMOUNT_TYPE = " REAL";
    private static final String CURRENCY_TYPE = " INTEGER";
    private static final String CATEGORY_TYPE = " INTEGER";
    private static final String PERIOD_TYPE = " INTEGER";
    private static final String DAY_TYPE = " INTEGER";
    private static final String MONTH_TYPE = " INTEGER";
    private static final String YEAR_TYPE = " INTEGER";
    private static final String HOUR_TYPE = " INTEGER";
    private static final String MINUTE_TYPE = " INTEGER";
    private static final String NOTES_TYPE = " TEXT";
    private static final String REPEATING_TYPE = " INTEGER";

    // TABLE 1
    public static final class RecordTable implements BaseColumns {

        // CONSTANTS
        public static final String TABLE_NAME = "Records";

        // Column
        public static final String KEY_RECORD_TYPE = "RecordType";
        public static final String KEY_INSERTION_TYPE = "InsertionType";
        public static final String KEY_AMOUNT = "Amount";
        public static final String KEY_CURRENCY = "Currency";
        public static final String KEY_CATEGORY = "Category";
        public static final String KEY_PERIOD = "Period";
        public static final String KEY_DAY = "Day";
        public static final String KEY_MONTH = "Month";
        public static final String KEY_YEAR = "Year";
        public static final String KEY_HOUR = "Hour";
        public static final String KEY_MINUTE = "Minute";
        public static final String KEY_NOTES = "Notes";
        public static final String KEY_REPEATING = "Repeating";

        public static final String[] ALL_COLUMNS = {
                _ID,
                KEY_RECORD_TYPE,
                KEY_INSERTION_TYPE,
                KEY_AMOUNT,
                KEY_CURRENCY,
                KEY_CATEGORY,
                KEY_PERIOD,
                KEY_DAY,
                KEY_MONTH,
                KEY_YEAR,
                KEY_HOUR,
                KEY_MINUTE,
                KEY_NOTES,
                KEY_REPEATING
        };

        public static final String CREATE_TABLE =
                "CREATE TABLE " + TABLE_NAME + "("
                        + _ID + " INTEGER PRIMARY KEY, "
                        + KEY_RECORD_TYPE + RECORD_TYPE + " NOT NULL, "
                        + KEY_INSERTION_TYPE + INSERTION_TYPE + " NOT NULL, "
                        + KEY_AMOUNT + AMOUNT_TYPE + " NOT NULL, "
                        + KEY_CURRENCY + CURRENCY_TYPE + " NOT NULL, "
                        + KEY_CATEGORY + CATEGORY_TYPE + " NOT NULL, "
                        + KEY_PERIOD + PERIOD_TYPE + ", "
                        + KEY_DAY + DAY_TYPE + " NOT NULL, "
                        + KEY_MONTH + MONTH_TYPE + " NOT NULL, "
                        + KEY_YEAR + YEAR_TYPE + " NOT NULL, "
                        + KEY_HOUR + HOUR_TYPE + " NOT NULL, "
                        + KEY_MINUTE + MINUTE_TYPE + " NOT NULL, "
                        + KEY_NOTES + NOTES_TYPE + ", "
                        + KEY_REPEATING + REPEATING_TYPE
                        + " );";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

    }
}