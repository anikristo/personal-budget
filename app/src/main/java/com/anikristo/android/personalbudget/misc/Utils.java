package com.anikristo.android.personalbudget.misc;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.anikristo.android.personalbudget.R;
import com.anikristo.android.personalbudget.database.DBContract;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * TODO
 */
public class Utils {

    // CONSTANTS
    public static final String INTENT_ACTION_RECORD_RESULT = "com.anikristo.android.personalbudget.records.action.RECORD_RESULT";
    public static final String INTENT_ACTION_NOTIFICATION_CONFIRM = "com.anikristo.android.personalbudget.misc.action.NOTIFICATION_CONFIRM";
    public static final String INTENT_ACTION_NOTIFICATION_CANCEL = "com.anikristo.android.personalbudget.misc.action.NOTIFICATION_CANCEL";
    public static final String INTENT_ACTION_NOTIFICATION = "com.anikristo.android.personalbudget.misc.action.NOTIFICATION";
    public static final String INTENT_ACTION_PERIODICAL_SERVICE = "com.anikristo.android.personalbudget.misc.action.PERIODICAL_SERVICE";

    public static final String INTENT_EXTRA_ORIGIN = "com.anikristo.android.personalbudget.misc.extra.ORIGIN";
    public static final String INTENT_EXTRA_ORIGIN_NOTIFICATION_AUTO_ADD = "com.anikristo.android.personalbudget.misc.extra.ORIGIN_NOTIFICATION_AUTO_ADD";
    public static final String INTENT_EXTRA_ORIGIN_NOTIFICATION_CONFIRM = "com.anikristo.android.personalbudget.misc.extra.ORIGIN_NOTIFICATION_CONFIRM";
    public static final String INTENT_EXTRA_RECORD_TYPE = "com.anikristo.android.personalbudget.misc.extra.RECORD_TYPE";
    public static final String INTENT_EXTRA_ROW_ID = "com.anikristo.android.personalbudget.misc.extra.ROW_ID";
    public static final String INTENT_EXTRA_AMOUNT = "com.anikristo.android.personalbudget.misc.extra.AMOUNT";
    public static final String INTENT_EXTRA_CURRENCY = "com.anikristo.android.personalbudget.misc.extra.CURRENCY";
    public static final String INTENT_EXTRA_PERIOD = "com.anikristo.android.personalbudget.misc.extra.PERIOD";
    public static final String INTENT_EXTRA_DAY = "com.anikristo.android.personalbudget.misc.extra.DAY";
    public static final String INTENT_EXTRA_MONTH = "com.anikristo.android.personalbudget.misc.extra.MONTH";
    public static final String INTENT_EXTRA_YEAR = "com.anikristo.android.personalbudget.misc.extra.YEAR";
    public static final String INTENT_EXTRA_HOUR = "com.anikristo.android.personalbudget.misc.extra.HOUR";
    public static final String INTENT_EXTRA_MINUTE = "com.anikristo.android.personalbudget.misc.extra.MINUTE";
    public static final String INTENT_EXTRA_NOTES = "com.anikristo.android.personalbudget.misc.extra.NOTES";
    public static final String INTENT_EXTRA_CATEGORY = "com.anikristo.android.personalbudget.misc.extra.CATEGORY";
    public static final String INTENT_EXTRA_FUNCTION = "com.anikristo.android.personalbudget.misc.extra.FUNCTION";
    public static final String INTENT_EXTRA_ADAPTER_POS = "com.anikristo.android.personalbudget.misc.extra.ADAPTER_POS";
    public static final String INTENT_EXTRA_INSERTION_TYPE = "com.anikristo.android.personalbudget.misc.extra.INSERTION_TYPE";
    public static final String INTENT_EXTRA_STATUS = "com.anikristo.android.personalbudget.misc.extra.STATUS";

    public static final int NOTIFICATION_ID_INCOME = 0x145e;
    public static final int NOTIFICATION_ID_EXPENSE = 0x145f;

    // METHODS
    public static String formatSum(double sum, int mantissa) {
        String format = "0";

        if (mantissa > 0) {
            format += ".";
            for (int i = 1; i <= mantissa; i++) {
                format += "#";
            }
        }

        String sumStr = new DecimalFormat(format).format(sum);

        if (sum >= 1000000000)
            sumStr = new DecimalFormat(format).format(sum / 1000000000) + "B";
        else if (sum >= 1000000)
            sumStr = new DecimalFormat(format).format(sum / 1000000) + "M";
        else if (sum >= 1000)
            sumStr = new DecimalFormat(format).format(sum / 1000) + "K";

        return sumStr;
    }

    public static String formatDate(int day, int month, int year) {
        return String.format("%02d / %02d / %4d", day, month, year);
    }

    public static String formatTime(int hour, int minute) {
        return String.format("%02d : %02d", hour, minute);
    }

    public static int[] getChartColorList(int numberOfColors, boolean headerColor) {
        return new int[]{R.color.accent_neutral, R.color.primary_neutral};
    }

    public static String getNotificationMessage(Context context, boolean confirmFirst, int recordType, Double amount, Integer currency, Integer day, Integer month, Integer year) {

        String amountStr = amount + CurrencyHolder.getCurrency(currency).getSymbol();
        String dateStr = formatDate(day, month, year);

        if (!confirmFirst) {
            if (recordType == DBContract.RECORD_INCOME) {
                return String.format(context.getString(R.string.msg_notification_new_record_auto), context.getString(R.string.income), amountStr, dateStr);
            } else {
                return String.format(context.getString(R.string.msg_notification_new_record_auto), context.getString(R.string.expense), amountStr, dateStr);
            }
        } else {
            if (recordType == DBContract.RECORD_INCOME) {
                return String.format(context.getString(R.string.msg_notification_new_record_confirm), context.getString(R.string.income), amountStr, dateStr);
            } else {
                return String.format(context.getString(R.string.msg_notification_new_record_confirm), context.getString(R.string.expense), amountStr, dateStr);
            }
        }
    }

    public static long getMillisPerDay() {
        return 86400000 / 24 / 60;
    }

    public static void registerAlarm(Context context, Bundle bundle) {

        // Creating the intent to be wrapped
        Intent intent = new Intent(Utils.INTENT_ACTION_PERIODICAL_SERVICE);
        intent.putExtras(bundle);

        // Pending Intent
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, (int) bundle.getLong(INTENT_EXTRA_ROW_ID), intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Determining when to ring
        long period = bundle.getInt(INTENT_EXTRA_PERIOD) * Utils.getMillisPerDay();

        Calendar now = Calendar.getInstance();
        long timeNow = now.getTimeInMillis();

        Calendar originalCal = new GregorianCalendar();
        originalCal.clear();
        originalCal.set(Calendar.DATE, bundle.getInt(Utils.INTENT_EXTRA_DAY));
        originalCal.set(Calendar.MONTH, bundle.getInt(Utils.INTENT_EXTRA_MONTH) - 1);
        originalCal.set(Calendar.YEAR, bundle.getInt(Utils.INTENT_EXTRA_YEAR));
        originalCal.set(Calendar.HOUR_OF_DAY, bundle.getInt(Utils.INTENT_EXTRA_HOUR));
        originalCal.set(Calendar.MINUTE, bundle.getInt(Utils.INTENT_EXTRA_MINUTE));
        long additionTime = originalCal.getTimeInMillis();

        long shift = period - ((timeNow - additionTime) % period);

        // Alarm Manager
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.setInexactRepeating(AlarmManager.RTC, timeNow + shift, period, pendingIntent);
    }
}
