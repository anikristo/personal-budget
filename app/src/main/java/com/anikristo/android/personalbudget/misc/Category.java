package com.anikristo.android.personalbudget.misc;

/**
 * Author: Ani Kristo
 * Version: 1.0
 * Date: 22-Feb-2015
 * <p/>
 * Description: This enumerator provides the inherent Expense Categories that will be provided
 * to the user as soon as they operate with the respective UI widget.
 */
public class Category {
    // PROPERTIES
    private final String name;
    private final int id;

    // CONSTRUCTOR
    Category(String name, int id) {
        this.name = name;
        this.id = id;
    }

    // METHODS
    @Override
    public String toString() {
        return name;
    }

    public int getID() {
        return id;
    }
}
