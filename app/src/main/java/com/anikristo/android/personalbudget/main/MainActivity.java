package com.anikristo.android.personalbudget.main;
/**
 * Author: Ani Kristo
 * Version: 1.0
 * Date: 22-Feb-2015
 * <p/>
 * Description: The main activity is the one which will launch as soon as the app opens. It is
 * indeed an activity based on a Navigation Drawer interface which provides different sections for
 * accessing information about the Incomes and Expenses.
 * <p/>
 * The first section will display My Incomes ... TODO
 * <p/>
 * The second section will display My Expenses ... TODO
 */

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.anikristo.android.personalbudget.R;
import com.anikristo.android.personalbudget.database.DBContract;
import com.anikristo.android.personalbudget.dialogs.NotificationFeedbackDialog;
import com.anikristo.android.personalbudget.misc.Utils;
import com.anikristo.android.personalbudget.records.RecordActivity;
import com.anikristo.android.personalbudget.settings.SettingsActivity;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {

    // CONSTANTS
    private static final int INCOMES_FRAGMENT_ID = R.id.drawer_menu_incomes;
    private static final int EXPENSES_FRAGMENT_ID = R.id.drawer_menu_expenses;
    private static final int HOME_FRAGMENT_ID = R.id.drawer_menu_home;
    private static final int STATISTICS_FRAGMENT_ID = R.id.drawer_menu_stats;
    private static final int PERIODS_FRAGMENT_ID = R.id.drawer_menu_periods;
    private static final int SETTINGS_FRAGMENT_ID = R.id.drawer_menu_settings;
    private static final int FEEDBACK_FRAGMENT_ID = R.id.drawer_menu_feedback;

    // PROPERTIES
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    private CircleImageView accountPic;
    private TextView nameTV;
    private TextView emailTV;

    private HomeFragment homeFrag;
    private IncomesFragment incomesFrag;
    private ExpensesFragment expensesFrag;
    private StatisticsFragment statFrag;
    private PeriodFragment periodsFrag;
    private FeedbackFragment feedbackFrag;

    private BroadcastReceiver resultReceiver;

    // METHODS
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);

        navigationView = (NavigationView) findViewById(R.id.drawer_navigationView);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                selectItem(menuItem);
                if (menuItem.getItemId() != SETTINGS_FRAGMENT_ID && menuItem.getItemId() != FEEDBACK_FRAGMENT_ID)
                    menuItem.setChecked(true);
                return true;
            }
        });

        accountPic = (CircleImageView) findViewById(R.id.header_profile_image);
        nameTV = (TextView) findViewById(R.id.header_username);
        emailTV = (TextView) findViewById(R.id.header_email);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
                invalidateOptionsMenu();
                syncState();
            }
        };

        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        // instantiating fragments to be shown
        homeFrag = new HomeFragment();
        incomesFrag = new IncomesFragment();
        expensesFrag = new ExpensesFragment();
        statFrag = new StatisticsFragment();
        periodsFrag = new PeriodFragment();
        feedbackFrag = new FeedbackFragment();

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction().replace(R.id.container, homeFrag).commit();
            navigationView.getMenu().findItem(HOME_FRAGMENT_ID).setChecked(true);
        }

        // Registering Broadcast Receivers
        resultReceiver = new ResultReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(resultReceiver, new IntentFilter(Utils.INTENT_ACTION_RECORD_RESULT));

        // Default values for Settings
        PreferenceManager.setDefaultValues(this, R.xml.pref, false);

        new ProfilePhotoTask().execute();

        onReceivedNotificationIntent(getIntent());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(resultReceiver);
    }

    @Override
    public void setTitle(CharSequence title) {
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(title);
    }

    private void selectItem(MenuItem menuItem) {
        Fragment fragment;
        int itemID = menuItem.getItemId();
        String title = menuItem.getTitle().toString();

        if (itemID == HOME_FRAGMENT_ID) {
            fragment = homeFrag;
        } else if (itemID == INCOMES_FRAGMENT_ID) { // Incomes
            fragment = incomesFrag;
        } else if (itemID == EXPENSES_FRAGMENT_ID) { // Expenses
            fragment = expensesFrag;
        } else if (itemID == STATISTICS_FRAGMENT_ID) { // Stats
            fragment = statFrag;
        } else if (itemID == PERIODS_FRAGMENT_ID) { // Periods
            fragment = periodsFrag;
        } else if (itemID == SETTINGS_FRAGMENT_ID) { // Setting
            // Start Settings Activity
            startActivity(new Intent(this, SettingsActivity.class));
            drawerLayout.closeDrawer(navigationView);
            return;
        } else if (itemID == FEEDBACK_FRAGMENT_ID) { // Feedback
            fragment = feedbackFrag;
        } else {
            return;
        }

        getFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();

        // update selected item and title, then close the drawer
        drawerLayout.closeDrawer(navigationView);
        setTitle(title);
    }

    public void onReceivedNotificationIntent(Intent intent) {
        if (intent.getAction() != null && intent.getAction().equals(Utils.INTENT_ACTION_NOTIFICATION)) {

            Bundle extras = intent.getExtras();
            int recordType = extras.getInt(Utils.INTENT_EXTRA_RECORD_TYPE);
            String origin = extras.getString(Utils.INTENT_EXTRA_ORIGIN);

            if (origin != null) {
                if (origin.equals(Utils.INTENT_EXTRA_ORIGIN_NOTIFICATION_AUTO_ADD)) {

                    if (recordType == DBContract.RECORD_INCOME)
                        showIncomes();
                    else if (recordType == DBContract.RECORD_EXPENSE)
                        showExpenses();

                } else if (origin.equals(Utils.INTENT_EXTRA_ORIGIN_NOTIFICATION_CONFIRM)) {
                    NotificationFeedbackDialog dialogFragment = new NotificationFeedbackDialog();
                    dialogFragment.setup(this, extras, homeFrag);
                    dialogFragment.show(getFragmentManager(), "NOTIFICATION_FEEDBACK_DIALOG");
                }
            }
        }
    }

    public void showIncomes() {
        getFragmentManager().beginTransaction().replace(R.id.container, incomesFrag).commit();
        setTitle(R.string.drawer_title_incomes);
        navigationView.getMenu().findItem(R.id.drawer_menu_incomes).setChecked(true);
    }

    public void showExpenses() {
        getFragmentManager().beginTransaction().replace(R.id.container, expensesFrag).commit();
        setTitle(R.string.drawer_title_expenses);
        navigationView.getMenu().findItem(R.id.drawer_menu_expenses).setChecked(true);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    class ResultReceiver extends BroadcastReceiver {

        // PROPERTIES
        private View view;

        // METHODS
        private void setView() {
            if (incomesFrag.isVisible()) {
                view = incomesFrag.getFAB();
            } else if (expensesFrag.isVisible()) {
                view = expensesFrag.getFAB();
            }

            if (view == null)
                view = getWindow().getDecorView();
        }

        @Override
        public void onReceive(Context context, final Intent intent) {

            setView();

            // Handling the event for Snackbars
            int status = intent.getIntExtra(Utils.INTENT_EXTRA_STATUS, RecordActivity.STATUS_NAV_UP);
            if (status == RecordActivity.STATUS_INSERTED) {

                if (intent.getIntExtra(Utils.INTENT_EXTRA_RECORD_TYPE, DBContract.RECORD_INCOME) == DBContract.RECORD_INCOME)
                    incomesFrag.itemAdded();
                else if (intent.getIntExtra(Utils.INTENT_EXTRA_RECORD_TYPE, DBContract.RECORD_INCOME) == DBContract.RECORD_EXPENSE)
                    expensesFrag.itemAdded();

                if (intent.getIntExtra(Utils.INTENT_EXTRA_PERIOD, 0) > 0
                        && intent.getIntExtra(Utils.INTENT_EXTRA_INSERTION_TYPE, DBContract.INSERTION_TYPE_AUTO) == (DBContract.INSERTION_TYPE_MANUAL)) {
                    Bundle extras = intent.getExtras();
                    extras.remove(Utils.INTENT_EXTRA_STATUS);
                    extras.remove(Utils.INTENT_EXTRA_ADAPTER_POS);
                    extras.remove(Utils.INTENT_EXTRA_FUNCTION);
                    Utils.registerAlarm(MainActivity.this, extras);
                }

                Snackbar.make(view, R.string.info_record_added, Snackbar.LENGTH_LONG)
                        .show();

            } else if (status == RecordActivity.STATUS_UNSUCCESSFUL) {
                Snackbar.make(view, R.string.info_unsuccessful, Snackbar.LENGTH_LONG)
                        .setAction(R.string.action_retry, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                // Start AddAndUpdateRecordActivity again with the entered info
                                Intent newIntent = new Intent(getApplicationContext(), RecordActivity.class);
                                newIntent.putExtras(intent.getExtras());
                                newIntent.removeExtra(Utils.INTENT_EXTRA_STATUS);
                                startActivity(newIntent);
                            }
                        })
                        .show();
            } else if (status == RecordActivity.STATUS_UPDATED) {

                final int position = intent.getIntExtra(Utils.INTENT_EXTRA_ADAPTER_POS, -1);

                if (intent.getIntExtra(Utils.INTENT_EXTRA_RECORD_TYPE, DBContract.RECORD_INCOME) == DBContract.RECORD_INCOME)
                    incomesFrag.itemUpdated(position);
                else if (intent.getIntExtra(Utils.INTENT_EXTRA_RECORD_TYPE, DBContract.RECORD_INCOME) == DBContract.RECORD_EXPENSE)
                    expensesFrag.itemUpdated(position);

                if (intent.getIntExtra(Utils.INTENT_EXTRA_PERIOD, 0) > 0
                        && intent.getIntExtra(Utils.INTENT_EXTRA_INSERTION_TYPE, DBContract.INSERTION_TYPE_AUTO) == (DBContract.INSERTION_TYPE_MANUAL)) {
                    Bundle extras = intent.getExtras();
                    extras.remove(Utils.INTENT_EXTRA_STATUS);
                    extras.remove(Utils.INTENT_EXTRA_ADAPTER_POS);
                    extras.remove(Utils.INTENT_EXTRA_FUNCTION);
                    Utils.registerAlarm(MainActivity.this, extras);
                }

                Snackbar snackbar = Snackbar.make(view, R.string.info_record_updated, Snackbar.LENGTH_LONG);

                if (intent.getIntExtra(Utils.INTENT_EXTRA_RECORD_TYPE, DBContract.RECORD_INCOME) == DBContract.RECORD_INCOME) {
                    snackbar.setAction(R.string.action_undo, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Undo update
                            incomesFrag.undoUpdate(intent.getLongExtra(Utils.INTENT_EXTRA_ROW_ID, -1), position);
                        }
                    });
                } else if (intent.getIntExtra(Utils.INTENT_EXTRA_RECORD_TYPE, DBContract.RECORD_INCOME) == DBContract.RECORD_EXPENSE) {
                    snackbar.setAction(R.string.action_undo, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Undo update
                            expensesFrag.undoUpdate(intent.getLongExtra(Utils.INTENT_EXTRA_ROW_ID, -1), position);
                        }
                    });
                }
                snackbar.show();
            }
        }
    }

    private class Profile {
        Uri imageURI;
        String fullName;
        String email;
    }

    private class ProfilePhotoTask extends AsyncTask<Void, Void, Profile> {

        @Override
        protected Profile doInBackground(Void... params) {

            try {
                // Sets the columns to retrieve for the user profile
                String[] projection = new String[]
                        {
                                ContactsContract.Profile.DISPLAY_NAME_PRIMARY,
                                ContactsContract.Profile.PHOTO_THUMBNAIL_URI
                        };

                // Retrieves the profile from the Contacts Provider
                Cursor profileCursor =
                        getContentResolver().query(
                                ContactsContract.Profile.CONTENT_URI,
                                projection,
                                null,
                                null,
                                null);
                profileCursor.moveToFirst();

                // Get the picture uri and display name
                String photoUri = profileCursor.getString(
                        profileCursor.getColumnIndexOrThrow(ContactsContract.Profile.PHOTO_THUMBNAIL_URI));
                String fullName = profileCursor.getString(
                        profileCursor.getColumnIndexOrThrow(ContactsContract.Profile.DISPLAY_NAME_PRIMARY));

                profileCursor.close();

                // Get email
                AccountManager accountManager = AccountManager.get(MainActivity.this);
                Account myAccount = accountManager.getAccounts()[0];


                Profile profile = new Profile();
                profile.imageURI = Uri.parse(photoUri);
                profile.fullName = fullName;
                profile.email = myAccount.name;
                return profile;

            } catch (NullPointerException e) {
                e.printStackTrace();

                return null;
            }
        }

        @Override
        protected void onPostExecute(Profile profile) {
            super.onPostExecute(profile);

            try {
                accountPic.setImageURI(profile.imageURI);
                nameTV.setText(profile.fullName);
                emailTV.setText(profile.email);

            } catch (Exception e) {
                e.printStackTrace();

                accountPic.setImageResource(R.drawable.ic_launcher);
                nameTV.setVisibility(View.GONE);
                emailTV.setVisibility(View.GONE);
            }
        }
    }
}