package com.anikristo.android.personalbudget.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import com.anikristo.android.personalbudget.R;

/**
 * TODO
 */
public class RecordLongClickDialog extends DialogFragment {

    // CONSTANTS
    public static final int OPTION_EDIT = 0x10;
    public static final int OPTION_DELETE = 0x11;

    // PROPERTIES
    private RecordLongClickDialogListener listener;

    // METHODS
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(R.array.menu_record_longClick, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int position) {
                if (position == 0)
                    listener.onSelectOption(OPTION_EDIT);
                else if (position == 1)
                    listener.onSelectOption(OPTION_DELETE);

                dismiss();
            }
        });
        Dialog d = builder.create();
        d.setCanceledOnTouchOutside(true);
        return d;
    }

    public void setListener(RecordLongClickDialogListener listener) {
        this.listener = listener;
    }

    // CUSTOM INTERFACE
    public interface RecordLongClickDialogListener {
        void onSelectOption(int option);
    }
}
