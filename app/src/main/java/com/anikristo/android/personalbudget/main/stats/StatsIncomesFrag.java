package com.anikristo.android.personalbudget.main.stats;

import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anikristo.android.personalbudget.R;
import com.anikristo.android.personalbudget.database.DBContract;
import com.anikristo.android.personalbudget.database.DBHelper;
import com.anikristo.android.personalbudget.misc.CategoryHolder;
import com.anikristo.android.personalbudget.misc.Utils;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.PercentFormatter;

import java.util.ArrayList;

/**
 * TODO
 */
public class StatsIncomesFrag extends Fragment {

    // PROPERTIES
    private PieChart chart;

    // METHODS
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_stats_incomes, container, false);
        chart = (PieChart) v.findViewById(R.id.stats_incomes_graph);

        new PopulatingTask().execute();
        return v;
    }

    private class PopulatingTask extends AsyncTask<Void, Void, Object[]> {

        @Override
        protected Object[] doInBackground(Void... params) {
            DBHelper dbHelper = new DBHelper(getActivity());
            double total = dbHelper.getTotalIncomes();

            // Get the sums for category and save the percentage
            ArrayList<Entry> entries = new ArrayList<>();
            ArrayList<String> xVals = new ArrayList<>();

            Cursor grouped = dbHelper.getAllIncomesGroupedByCategory();
            for (int i = 0; !grouped.isAfterLast(); grouped.moveToNext(), i++) {
                double value = grouped.getDouble(grouped.getColumnIndexOrThrow(DBContract.RecordTable.KEY_AMOUNT));
                float percentage = (float) (value / total * 100);
                entries.add(new Entry(percentage, i));

                int category = grouped.getInt(grouped.getColumnIndexOrThrow(DBContract.RecordTable.KEY_CATEGORY));
                xVals.add(CategoryHolder.getCategory(category, DBContract.RECORD_INCOME).toString());
            }

            return new Object[]{entries, xVals};
        }

        @Override
        @SuppressWarnings("unchecked")
        protected void onPostExecute(Object[] objects) {
            super.onPostExecute(objects);

            ArrayList<Entry> entries = (ArrayList<Entry>) objects[0];
            ArrayList<String> xVals = (ArrayList<String>) objects[1];

            PieDataSet dataSet = new PieDataSet(entries, null);
            dataSet.setSliceSpace(2);
            dataSet.setValueTextColor(getResources().getColor(android.R.color.white));
            dataSet.setColors(Utils.getChartColorList(CategoryHolder.getIncomeCategories().size(), false), getActivity());

            PieData data = new PieData(xVals, dataSet);
            data.setValueFormatter(new PercentFormatter());
            chart.setData(data);

            chart.getLegend().setEnabled(false);
            chart.setDrawHoleEnabled(false);
            chart.setDescription(null);
        }
    }

}
