package com.anikristo.android.personalbudget.main.stats;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.anikristo.android.personalbudget.R;

/**
 * TODO
 */
public class StatsPagerAdapter extends FragmentPagerAdapter {


    // PROPERTIES
    private Context context;

    // CONSTRUCTOR
    public StatsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    // METHODS
    @Override
    public Fragment getItem(int position) {
        if (position == 0)
            return new StatsOverviewFrag();
        else if (position == 1)
            return new StatsIncomesFrag();
        else if (position == 2)
            return new StatsExpensesFrag();
        else if (position == 3)
            return new StatsTrendsFrag();
        else
            return null;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0)
            return context.getString(R.string.stats_tab1);
        else if (position == 1)
            return context.getString(R.string.stats_tab2);
        else if (position == 2)
            return context.getString(R.string.stats_tab3);
        else if (position == 3)
            return context.getString(R.string.stats_tab4);
        else
            return null;
    }
}
