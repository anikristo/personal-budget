package com.anikristo.android.personalbudget.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.anikristo.android.personalbudget.R;
import com.anikristo.android.personalbudget.misc.Currency;
import com.anikristo.android.personalbudget.misc.CurrencyHolder;

/**
 * TODO
 */
public class CurrencyPickerDialog extends DialogFragment {


    // PROPERTIES
    private CurrencyDialogInterface listener;

    // METHODS
    @Override
    public void onAttach(Activity activity) {

        super.onAttach(activity);
        try {
            listener = (CurrencyDialogInterface) activity;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_currency, container, false);

        ListView listView = (ListView) view.findViewById(R.id.currencyDialog_listView);
        listView.setAdapter(new CurrencyListAdapter(getActivity()));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                listener.onCurrencyPicked(position);
                dismiss();
            }
        });
        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    // CUSTOM INTERFACE
    public interface CurrencyDialogInterface {
        void onCurrencyPicked(int position);
    }

    private static class CurrencyListAdapter extends ArrayAdapter<Currency> {

        // PROPERTIES
        final Context context;

        // CONSTRUCTOR
        public CurrencyListAdapter(Context context) {
            super(context, R.layout.fragment_dialog_currency, CurrencyHolder.getAllCurrencies());
            this.context = context;
        }

        // METHODS
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) { // Create new view
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = inflater.inflate(R.layout.item_currency_dialog, null);

                // Finding the text views
                TextView symbolTV = (TextView) v.findViewById(R.id.item_currencyDialog_symbol);
                TextView abbrTV = (TextView) v.findViewById(R.id.item_currencyDialog_abbr);
                TextView nameTV = (TextView) v.findViewById(R.id.item_currencyDialog_fullName);

                // Filling the TVs with the relevant info
                symbolTV.setText(CurrencyHolder.getCurrency(position).getSymbol());
                abbrTV.setText(CurrencyHolder.getCurrency(position).getAbbreviation());
                nameTV.setText(CurrencyHolder.getCurrency(position).getFullName());

                return v;

            } else { // Recycle the old view

                // Finding the text views
                TextView symbolTV = (TextView) convertView.findViewById(R.id.item_currencyDialog_symbol);
                TextView abbrTV = (TextView) convertView.findViewById(R.id.item_currencyDialog_abbr);
                TextView nameTV = (TextView) convertView.findViewById(R.id.item_currencyDialog_fullName);

                // Filling the TVs with the relevant info
                symbolTV.setText(CurrencyHolder.getCurrency(position).getSymbol());
                abbrTV.setText(CurrencyHolder.getCurrency(position).getAbbreviation());
                nameTV.setText(CurrencyHolder.getCurrency(position).getFullName());

                return convertView;
            }
        }
    }
}