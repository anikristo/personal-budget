package com.anikristo.android.personalbudget.main;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anikristo.android.personalbudget.R;
import com.anikristo.android.personalbudget.main.stats.StatsPagerAdapter;

public class StatisticsFragment extends Fragment {

    // METHODS
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_main_statistics, container, false);
        TabLayout tabs = (TabLayout) v.findViewById(R.id.stats_tabs);
        ViewPager viewpager = (ViewPager) v.findViewById(R.id.stats_viewpager);

        // Setting up
        viewpager.setAdapter(new StatsPagerAdapter(
                        getActivity(),
                        ((AppCompatActivity) getActivity()).getSupportFragmentManager())
        );
        tabs.setTabGravity(TabLayout.GRAVITY_CENTER);
        tabs.setupWithViewPager(viewpager);

        return v;
    }
}