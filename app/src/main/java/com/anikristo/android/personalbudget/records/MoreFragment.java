package com.anikristo.android.personalbudget.records;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.anikristo.android.personalbudget.R;
import com.anikristo.android.personalbudget.database.DBContract;
import com.anikristo.android.personalbudget.misc.Utils;
import com.anikristo.android.personalbudget.misc.interfaces.IMoreFragment;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;

public class MoreFragment extends Fragment
        implements IMoreFragment, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    // Widgets
    private EditText notesField;
    private SwitchCompat periodicalSwitch;
    private EditText periodField;
    private TextView periodHintTV;
    private Spinner freqSpinner;
    private EditText dateField;
    private EditText timeField;

    // updated info
    private Integer newPeriod;
    private Integer day;
    private Integer month;
    private Integer year;
    private Integer hour;
    private Integer minute;
    private String newNotes;

    // Methods
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_record_more, container, false);


        notesField = (EditText) v.findViewById(R.id.records_more_notes_et);
        periodicalSwitch = (SwitchCompat) v.findViewById(R.id.record_periodical);
        periodField = (EditText) v.findViewById(R.id.record_periodical_period);
        freqSpinner = (Spinner) v.findViewById(R.id.record_periodical_frequency_spinner);
        periodHintTV = (TextView) v.findViewById(R.id.record_main_periodical_hint);
        dateField = (EditText) v.findViewById(R.id.records_more_date);
        timeField = (EditText) v.findViewById(R.id.records_more_time);

        setPeriodFieldEnabled(false);
        periodicalSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // Display the extra widgets for frequency and auto mode
                setPeriodFieldEnabled(isChecked);
            }
        });

        // Sets default value of the disabled text field to the current date
        dateField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog datePicker = DatePickerDialog.newInstance(
                        MoreFragment.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH));
                datePicker.show(getActivity().getFragmentManager(), "DATE_DIALOG");
            }
        });

        // Sets default value of the disabled text field to the current time
        timeField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                TimePickerDialog timePicker = TimePickerDialog.newInstance(
                        MoreFragment.this,
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE),
                        true);
                timePicker.show(getFragmentManager(), "TIME_DIALOG");
            }
        });

        stylizeAndPopulate();
        return v;
    }

    private void setPeriodFieldEnabled(boolean isChecked) {
        periodField.setEnabled(isChecked);
        freqSpinner.setEnabled(isChecked);
        periodHintTV.setEnabled(isChecked);
    }

    @Override
    public String getNotes() {
        String str = notesField.getText().toString();
        if (str.isEmpty())
            return null;
        return str;
    }

    @Override
    public void setNotes(String notes) {
        newNotes = notes;
    }

    @Override
    public Integer getPeriod() {
        if (periodicalSwitch.isChecked()
                && freqSpinner != null
                && periodField != null
                && !periodField.getText().toString().isEmpty()) {
            int periodNr = Integer.parseInt(periodField.getText().toString());
            int freqSpinnerPos = freqSpinner.getSelectedItemPosition();

            if (freqSpinnerPos == 0) // Days
                return periodNr;
            if (freqSpinnerPos == 1) // Weeks
                return periodNr * 7;
            if (freqSpinnerPos == 2) // Months
                return periodNr * 30;
            if (freqSpinnerPos == 3) // Years
                return periodNr * 365;
            return null;
        }
        return null;
    }

    @Override
    public void setPeriod(int days) {
        newPeriod = days;
    }

    @Override
    public Integer getDay() {
        if (day != null)
            return day;
        else return Calendar.getInstance().get(Calendar.DATE);
    }

    @Override
    public Integer getMonth() {
        if (month != null)
            return month;
        else return Calendar.getInstance().get(Calendar.MONTH) + 1;
    }

    @Override
    public Integer getYear() {
        if (year != null)
            return year;
        else return Calendar.getInstance().get(Calendar.YEAR);
    }

    @Override
    public void setDate(Integer day, Integer month, Integer year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    @Override
    public Integer getHour() {
        if (hour != null)
            return hour;
        else return Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
    }

    @Override
    public Integer getMinute() {
        if (minute != null)
            return minute;
        else return Calendar.getInstance().get(Calendar.MINUTE);
    }

    @Override
    public void setTime(Integer hour, Integer minute) {
        this.hour = hour;
        this.minute = minute;
    }

    @Override
    public boolean contains(int field) {
        if (field == DATE)
            return true;
        if (field == TIME)
            return true;
        if (field == PERIOD)
            return periodicalSwitch.isChecked() && getPeriod() != null && getPeriod() != 0;
        if (field == NOTES)
            return !notesField.getText().toString().isEmpty();
        return false;
    }

    @Override
    public void stylizeAndPopulate() {

        int recordType = getActivity().getIntent().getIntExtra(Utils.INTENT_EXTRA_RECORD_TYPE, DBContract.RECORD_INCOME);

        dateField.setText(Utils.formatDate(getDay(), getMonth(), getYear()));
        timeField.setText(Utils.formatTime(getHour(), getMinute()));

        if (newNotes != null)
            notesField.setText(newNotes);

        if (newPeriod != null && newPeriod != 0) {
            setPeriodFieldEnabled(true);
            periodicalSwitch.setChecked(true);
            periodField.setText(newPeriod + "");
        }

        // Coloring
        int color;
        if (recordType == DBContract.RECORD_EXPENSE) {
            color = getResources().getColor(R.color.accent_expenses);
            periodHintTV.setText(R.string.subtitle_frequency_expense);
        } else {
            color = getResources().getColor(R.color.accent_incomes);
            periodHintTV.setText(R.string.subtitle_frequency_income);
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        dateField.setText(Utils.formatDate(dayOfMonth, monthOfYear + 1, year));
        setDate(dayOfMonth, monthOfYear + 1, year);
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        timeField.setText(Utils.formatTime(hourOfDay, minute));
        setTime(hourOfDay, minute);
    }

    public boolean isPeriodSwitchOn() {
        return periodicalSwitch.isChecked();
    }

    public void focusPeriodField() {
        if (periodField != null)
            periodField.requestFocus();
    }
}
