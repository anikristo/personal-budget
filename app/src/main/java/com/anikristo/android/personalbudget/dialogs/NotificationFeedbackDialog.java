package com.anikristo.android.personalbudget.dialogs;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import com.anikristo.android.personalbudget.R;
import com.anikristo.android.personalbudget.database.DBContract;
import com.anikristo.android.personalbudget.database.DBHelper;
import com.anikristo.android.personalbudget.main.HomeFragment;
import com.anikristo.android.personalbudget.misc.CategoryHolder;
import com.anikristo.android.personalbudget.misc.CurrencyHolder;
import com.anikristo.android.personalbudget.misc.Utils;

import java.util.Calendar;

/**
 * toDO
 */
public class NotificationFeedbackDialog extends DialogFragment {

    // PROPERTIES
    private int recordType;
    private double amount;
    private int currency;
    private int category;
    private int period;
    private String notes;
    private int day;
    private int month;
    private int year;
    private int hour;
    private int minute;
    private long rowID;

    private int oldDay;
    private int oldMonth;
    private int oldYear;

    private Context context;
    private HomeFragment homeFrag;

    // METHODS
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(Utils.getNotificationMessage(
                        getActivity(),
                        true,
                        recordType,
                        amount,
                        currency,
                        oldDay,
                        oldMonth,
                        oldYear)
        );
        builder.setPositiveButton(R.string.action_confirm, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Inserting
                DBHelper helper = new DBHelper(context);
                if (recordType == DBContract.RECORD_INCOME)
                    helper.insertIncomeRecord(
                            DBContract.INSERTION_TYPE_AUTO,
                            amount,
                            CurrencyHolder.getCurrency(currency),
                            CategoryHolder.getCategory(category, recordType),
                            period,
                            day,
                            month,
                            year,
                            hour,
                            minute,
                            notes
                    );
                else
                    helper.insertExpenseRecord(
                            DBContract.INSERTION_TYPE_AUTO,
                            amount,
                            CurrencyHolder.getCurrency(currency),
                            CategoryHolder.getCategory(category, recordType),
                            period,
                            day,
                            month,
                            year,
                            hour,
                            minute,
                            notes
                    );

                homeFrag.refresh();
            }
        });
        builder.setNegativeButton(R.string.action_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((AlarmManager) context.getSystemService(Context.ALARM_SERVICE))
                        .cancel(
                                PendingIntent.getBroadcast(
                                        context,
                                        (int) rowID,
                                        new Intent(Utils.INTENT_ACTION_PERIODICAL_SERVICE),
                                        PendingIntent.FLAG_UPDATE_CURRENT)
                        );

                homeFrag.refresh();
            }
        });

        return builder.create();
    }

    public DialogFragment setup(Context context, Bundle extras, HomeFragment homeFragment) {

        this.context = context;
        this.homeFrag = homeFragment;

        this.recordType = extras.getInt(Utils.INTENT_EXTRA_RECORD_TYPE);
        this.amount = extras.getDouble(Utils.INTENT_EXTRA_AMOUNT);
        this.currency = extras.getInt(Utils.INTENT_EXTRA_CURRENCY);
        this.notes = extras.getString(Utils.INTENT_EXTRA_NOTES);
        this.period = extras.getInt(Utils.INTENT_EXTRA_PERIOD);
        this.category = extras.getInt(Utils.INTENT_EXTRA_CATEGORY);
        this.rowID = extras.getInt(Utils.INTENT_EXTRA_ROW_ID);

        this.oldDay = extras.getInt(Utils.INTENT_EXTRA_DAY);
        this.oldMonth = extras.getInt(Utils.INTENT_EXTRA_MONTH);
        this.oldYear = extras.getInt(Utils.INTENT_EXTRA_YEAR);

        // Getting the properties
        Calendar c = Calendar.getInstance();
        this.day = c.get(Calendar.DAY_OF_MONTH);
        this.month = c.get(Calendar.MONTH) + 1;
        this.year = c.get(Calendar.YEAR);
        this.hour = c.get(Calendar.HOUR_OF_DAY);
        this.minute = c.get(Calendar.MINUTE);

        return this;
    }
}
