package com.anikristo.android.personalbudget.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.anikristo.android.personalbudget.R;
import com.anikristo.android.personalbudget.misc.Category;
import com.anikristo.android.personalbudget.misc.CategoryHolder;

/**
 * TODO
 */
public class CategoryPickerDialog extends DialogFragment {

    // PROPERTIES
    private int recordType;
    private CategoryPickerDialogListener listener;

    // METHODS
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_category, container, false);

        ListView listView = (ListView) view.findViewById(R.id.categoryDialog_listView);
        listView.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.item_category_dialog, CategoryHolder.getAllCategories(recordType)));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                listener.onCategoryPicked(CategoryHolder.getCategory(position, recordType));
                dismiss();
            }
        });
        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    public void setListener(CategoryPickerDialogListener listener) {
        this.listener = listener;
    }

    public void setRecordType(int recordType) {
        this.recordType = recordType;
    }

    // CUSTOM INTERFACE
    public interface CategoryPickerDialogListener {
        void onCategoryPicked(Category category);
    }
}
