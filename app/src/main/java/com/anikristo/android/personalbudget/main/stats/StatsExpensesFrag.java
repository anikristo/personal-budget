package com.anikristo.android.personalbudget.main.stats;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anikristo.android.personalbudget.R;

/**
 * TODO
 */
public class StatsExpensesFrag extends Fragment {

    // METHODS
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_stats_expenses, container, false);
    }
}
