package com.anikristo.android.personalbudget.misc;

import android.content.Context;
import android.telephony.TelephonyManager;

import java.util.Arrays;
import java.util.List;

/**
 * TODO
 */
public class CurrencyHolder {

    // CONSTANTS
    public static final Currency USD = new Currency("$", "USD", "US Dollar", "us");
    public static final Currency EUR = new Currency("\u20AC", "EUR", "Euro",
            "ad", "at", "be", "cy", "ee", "fi", "fr", "de", "gr", "ie", "it", "xk", "lv", "lt",
            "lu", "mt", "mc", "me", "nl", "pt", "sm", "sk", "si", "es", "va");
    public static final Currency ALL = new Currency("L", "ALL", "Albanian Lek", "al");
    public static final Currency TRY = new Currency("TL", "TRY", "Turkish Lira", "tr");
    public static final Currency UNDEFINED = new Currency("?", "???", "???", "??");

    private static final Currency[] ALL_CURRENCIES = {USD, EUR, ALL, TRY};

    // METHODS
    public static List<Currency> getAllCurrencies() {
        return Arrays.asList(ALL_CURRENCIES);
    }

    public static Currency getCurrency(int position) {
        if (position == 0)
            return USD;
        else if (position == 1)
            return EUR;
        else if (position == 2)
            return ALL;
        else if (position == 3)
            return TRY;
        else return UNDEFINED;
    }

    public static int getPosition(Currency currency) {
        if (currency == null)
            return -1;
        else if (currency == USD)
            return 0;
        else if (currency == EUR)
            return 1;
        else if (currency == ALL)
            return 2;
        else if (currency == TRY)
            return 3;
        else
            return -1;
    }

    public static Currency getDefaultCurrency(Context context) {
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String countryISO = manager.getSimCountryIso();

        for (Currency c : ALL_CURRENCIES) {
            if (c.hasCountryISO(countryISO))
                return c;
        }

        return USD;
    }
}
