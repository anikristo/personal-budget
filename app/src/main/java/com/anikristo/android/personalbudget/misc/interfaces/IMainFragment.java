package com.anikristo.android.personalbudget.misc.interfaces;

import com.anikristo.android.personalbudget.misc.Category;
import com.anikristo.android.personalbudget.misc.Currency;

public interface IMainFragment extends IBaseFragment {
    int AMOUNT = 0;
    int CURRENCY = 1;
    int CATEGORY = 7;

    Category getCategory();

    void setCategory(int category);

    double getAmount();

    void setAmount(double amount);

    Currency getCurrency();

    void setCurrency(int currency);

    void showAmountErrorTV(boolean show);
}
