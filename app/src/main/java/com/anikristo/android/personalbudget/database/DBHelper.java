package com.anikristo.android.personalbudget.database;
/**
 * Author: Ani Kristo
 * Version: 1.0
 * Date: 22-Feb-2015
 * <p/>
 * Description: DBHelper stands for Database Helper. This class extends the API's class
 * SQLiteOpenHelper and provides the bridge connection between the SQL database and the app.
 * It is used to access the database by means of some insertion / query / update / delete methods
 * adapted to the requirements of the app.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.anikristo.android.personalbudget.main.HomeFragment;
import com.anikristo.android.personalbudget.misc.Category;
import com.anikristo.android.personalbudget.misc.Currency;
import com.anikristo.android.personalbudget.misc.CurrencyHolder;

public class DBHelper extends SQLiteOpenHelper {

    // PROPERTIES
    private final SQLiteDatabase wdb;
    private final SQLiteDatabase rdb;

    // CONSTRUCTOR 
    public DBHelper(Context context) {
        super(context, DBContract.DB_NAME, null, DBContract.DB_VERSION);

        // Opens the database, because the constructor simply does not actually create a DB
        wdb = getWritableDatabase();
        rdb = getReadableDatabase();
    }

    // METHODS
    @Override
    public void onCreate(SQLiteDatabase db) {
        for (int i = 0; i < DBContract.NUMBER_OF_TABLES; i++) {
            // create all tables
            db.execSQL(DBContract.CREATE_ALL_TABLES[i]);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (int i = 0; i < DBContract.NUMBER_OF_TABLES; i++) {
            // delete all records and restore the empty tables
            db.execSQL(DBContract.DROP_ALL_TABLES[i]);
        }
        onCreate(db);
    }

    // INSERT METHODS
    public long insertIncomeRecord(Integer insertionType, Double amount, Currency currency, Category category, Integer period, Integer day,
                                   Integer month, Integer year, Integer hour, Integer minute,
                                   String comments) {

        // Currency conversion
        int currencyValue = CurrencyHolder.getPosition(currency);

        // Content Values
        ContentValues values = new ContentValues();
        values.put(DBContract.RecordTable.KEY_RECORD_TYPE, DBContract.RECORD_INCOME);
        values.put(DBContract.RecordTable.KEY_INSERTION_TYPE, insertionType);
        values.put(DBContract.RecordTable.KEY_AMOUNT, amount);
        values.put(DBContract.RecordTable.KEY_CURRENCY, currencyValue);
        values.put(DBContract.RecordTable.KEY_CATEGORY, category.getID());
        values.put(DBContract.RecordTable.KEY_PERIOD, period);
        values.put(DBContract.RecordTable.KEY_DAY, day);
        values.put(DBContract.RecordTable.KEY_MONTH, month);
        values.put(DBContract.RecordTable.KEY_YEAR, year);
        values.put(DBContract.RecordTable.KEY_HOUR, hour);
        values.put(DBContract.RecordTable.KEY_MINUTE, minute);
        values.put(DBContract.RecordTable.KEY_NOTES, comments);
        values.put(DBContract.RecordTable.KEY_REPEATING, (period != null && period > 0) ? DBContract.REPEATING_ON : DBContract.REPEATING_OFF);

        // Writing to database
        return wdb.insert(DBContract.RecordTable.TABLE_NAME, null, values);
    }

    public long insertExpenseRecord(Integer insertionType, Double amount, Currency currency, Category category,
                                    Integer period, Integer day, Integer month, Integer year,
                                    Integer hour, Integer minute, String comments) {

        // Category Conversion
        int categoryNumber = category.getID();

        // Currency conversion
        int currencyValue = CurrencyHolder.getPosition(currency);

        // Content Values
        ContentValues values = new ContentValues();
        values.put(DBContract.RecordTable.KEY_RECORD_TYPE, DBContract.RECORD_EXPENSE);
        values.put(DBContract.RecordTable.KEY_INSERTION_TYPE, insertionType);
        values.put(DBContract.RecordTable.KEY_AMOUNT, amount);
        values.put(DBContract.RecordTable.KEY_CURRENCY, currencyValue);
        values.put(DBContract.RecordTable.KEY_CATEGORY, categoryNumber);
        values.put(DBContract.RecordTable.KEY_PERIOD, period);
        values.put(DBContract.RecordTable.KEY_DAY, day);
        values.put(DBContract.RecordTable.KEY_MONTH, month);
        values.put(DBContract.RecordTable.KEY_YEAR, year);
        values.put(DBContract.RecordTable.KEY_HOUR, hour);
        values.put(DBContract.RecordTable.KEY_MINUTE, minute);
        values.put(DBContract.RecordTable.KEY_NOTES, comments);
        values.put(DBContract.RecordTable.KEY_REPEATING, (period != null && period > 0) ? DBContract.REPEATING_ON : DBContract.REPEATING_OFF);

        // Writing to database
        return wdb.insert(DBContract.RecordTable.TABLE_NAME, null, values);
    }

    // QUERY METHODS
    public Cursor getAllIncomesDesc() {

        String tableName = DBContract.RecordTable.TABLE_NAME;
        String whereClause = DBContract.RecordTable.KEY_RECORD_TYPE + " = ?";
        String[] whereArgs = {DBContract.RECORD_INCOME + ""};
        String orderBy = DBContract.RecordTable._ID + " DESC";

        Cursor c = rdb.query(tableName, null, whereClause, whereArgs, null, null, orderBy, null);
        c.moveToFirst();
        return c;
    }

    public Cursor getIncomeRecord(long rowID) {

        String whereClause = DBContract.RecordTable._ID + " = ? AND " + DBContract.RecordTable.KEY_RECORD_TYPE + " = ?";
        String[] whereArgs = {rowID + "", DBContract.RECORD_INCOME + ""};

        Cursor c = rdb.query(DBContract.RecordTable.TABLE_NAME, null, whereClause, whereArgs, null, null, null, null);
        c.moveToFirst();
        return c;
    }

    public Cursor getAllExpensesDesc() {

        String tableName = DBContract.RecordTable.TABLE_NAME;
        String whereClause = DBContract.RecordTable.KEY_RECORD_TYPE + " = ?";
        String[] whereArgs = {DBContract.RECORD_EXPENSE + ""};
        String orderBy = DBContract.RecordTable._ID + " DESC";

        Cursor c = rdb.query(tableName, null, whereClause, whereArgs, null, null, orderBy, null);
        c.moveToFirst();
        return c;
    }

    public double getTotalIncomes() {

        String[] columns = {
                DBContract.RecordTable._ID,
                DBContract.RecordTable.KEY_RECORD_TYPE,
                "SUM(" + DBContract.RecordTable.KEY_AMOUNT + ")"
        };

        String whereClause = DBContract.RecordTable.KEY_RECORD_TYPE + " = ?";
        String[] whereArgs = {DBContract.RECORD_INCOME + ""};

        String groupBy = DBContract.RecordTable.KEY_RECORD_TYPE;

        Cursor c = rdb.query(DBContract.RecordTable.TABLE_NAME, columns, whereClause, whereArgs, groupBy, null, null, null);
        c.moveToFirst();

        double sum = 0;
        if (c.getCount() != 0) {
            sum = c.getDouble(c.getColumnIndexOrThrow("SUM(" + DBContract.RecordTable.KEY_AMOUNT + ")"));
        }
        c.close();

        return sum;
    }

    public double getTotalExpenses() {

        String[] columns = {
                DBContract.RecordTable._ID,
                DBContract.RecordTable.KEY_CATEGORY,
                "SUM(" + DBContract.RecordTable.KEY_AMOUNT + ")"
        };

        String whereClause = DBContract.RecordTable.KEY_RECORD_TYPE + " = ?";
        String[] whereArgs = {DBContract.RECORD_EXPENSE + ""};

        String groupBy = DBContract.RecordTable.KEY_RECORD_TYPE;

        Cursor c = rdb.query(DBContract.RecordTable.TABLE_NAME, columns, whereClause, whereArgs, groupBy, null, null, null);
        c.moveToFirst();

        double sum = 0;
        if (c.getCount() != 0) {
            sum = c.getDouble(c.getColumnIndexOrThrow("SUM(" + DBContract.RecordTable.KEY_AMOUNT + ")"));
        }
        c.close();

        return sum;
    }

    public Cursor getAllIncomesGroupedByCategory() {

        String whereClause = DBContract.RecordTable.KEY_RECORD_TYPE + " = ?";
        String[] whereArgs = {DBContract.RECORD_INCOME + ""};

        String groupBy = DBContract.RecordTable.KEY_CATEGORY;

        Cursor c = rdb.query(DBContract.RecordTable.TABLE_NAME, null, whereClause, whereArgs, groupBy, null, null);
        c.moveToFirst();
        return c;
    }

    public Cursor getAllExpensesGroupedByCategory() {

        String whereClause = DBContract.RecordTable.KEY_RECORD_TYPE + " = ?";
        String[] whereArgs = {DBContract.RECORD_EXPENSE + ""};

        String groupBy = DBContract.RecordTable.KEY_CATEGORY;

        Cursor c = rdb.query(DBContract.RecordTable.TABLE_NAME, null, whereClause, whereArgs, groupBy, null, null);
        c.moveToFirst();
        return c;
    }


    public Cursor getExpenseRecord(long rowID) {

        String whereClause = DBContract.RecordTable._ID + " = ? AND " + DBContract.RecordTable.KEY_RECORD_TYPE + " = ?";
        String[] whereArgs = {rowID + "", DBContract.RECORD_EXPENSE + ""};

        Cursor c = rdb.query(DBContract.RecordTable.TABLE_NAME, null, whereClause, whereArgs, null, null, null, null);
        c.moveToFirst();

        return c;
    }

    public Cursor getRecentRecords() {
        String tableName = DBContract.RecordTable.TABLE_NAME;
        String[] columns = DBContract.RecordTable.ALL_COLUMNS;
        String orderBy = DBContract.RecordTable.KEY_YEAR + " DESC, "
                + DBContract.RecordTable.KEY_MONTH + " DESC, "
                + DBContract.RecordTable.KEY_DAY + " DESC, "
                + DBContract.RecordTable._ID + " DESC";

        Cursor c = rdb.query(tableName, columns, null, null, null, null, orderBy, HomeFragment.RECENT_RECORDS_LIMIT + "");
        c.moveToFirst();
        return c;
    }

    // DELETE METHODS
    public boolean deleteIncomeRecord(long rowID) {
        String whereClause = DBContract.RecordTable._ID + " = ? AND " + DBContract.RecordTable.KEY_RECORD_TYPE + " = ?";
        String[] whereArgs = {rowID + "", DBContract.RECORD_INCOME + ""};
        return wdb.delete(DBContract.RecordTable.TABLE_NAME, whereClause, whereArgs) != 0;
    }

    public boolean deleteExpenseRecord(long rowID) {
        String whereClause = DBContract.RecordTable._ID + " = ? AND " + DBContract.RecordTable.KEY_RECORD_TYPE + " = ?";
        String[] whereArgs = {rowID + "", DBContract.RECORD_EXPENSE + ""};
        return wdb.delete(DBContract.RecordTable.TABLE_NAME, whereClause, whereArgs) != 0;
    }

    // UPDATE METHODS
    public boolean updateIncomeRecord(long rowID, double amount, Currency currency, Category category, Integer period,
                                      Integer day, Integer month, Integer year, Integer hour,
                                      Integer minute, String comments) {

        ContentValues values = new ContentValues();
        values.put(DBContract.RecordTable.KEY_AMOUNT, amount);
        values.put(DBContract.RecordTable.KEY_CURRENCY, CurrencyHolder.getPosition(currency));
        values.put(DBContract.RecordTable.KEY_CATEGORY, category.getID());
        values.put(DBContract.RecordTable.KEY_PERIOD, period);
        values.put(DBContract.RecordTable.KEY_DAY, day);
        values.put(DBContract.RecordTable.KEY_MONTH, month);
        values.put(DBContract.RecordTable.KEY_YEAR, year);
        values.put(DBContract.RecordTable.KEY_HOUR, hour);
        values.put(DBContract.RecordTable.KEY_MINUTE, minute);
        values.put(DBContract.RecordTable.KEY_NOTES, comments);
        values.put(DBContract.RecordTable.KEY_REPEATING, period > 0 ? DBContract.REPEATING_ON : DBContract.REPEATING_OFF);

        String whereClause = DBContract.RecordTable._ID + " = ? AND " + DBContract.RecordTable.KEY_RECORD_TYPE + " = ?";
        String[] whereArgs = {rowID + "", DBContract.RECORD_INCOME + ""};

        int result = wdb.update(DBContract.RecordTable.TABLE_NAME, values, whereClause, whereArgs);
        return result == 1;
    }

    public boolean updateExpenseRecord(long rowID, double amount, Currency currency,
                                       Category category, Integer period, Integer day,
                                       Integer month, Integer year, Integer hour, Integer minute,
                                       String comments) {

        ContentValues values = new ContentValues();
        values.put(DBContract.RecordTable.KEY_AMOUNT, amount);
        values.put(DBContract.RecordTable.KEY_CURRENCY, CurrencyHolder.getPosition(currency));
        values.put(DBContract.RecordTable.KEY_CATEGORY, category.getID());
        values.put(DBContract.RecordTable.KEY_PERIOD, period);
        values.put(DBContract.RecordTable.KEY_DAY, day);
        values.put(DBContract.RecordTable.KEY_MONTH, month);
        values.put(DBContract.RecordTable.KEY_YEAR, year);
        values.put(DBContract.RecordTable.KEY_HOUR, hour);
        values.put(DBContract.RecordTable.KEY_MINUTE, minute);
        values.put(DBContract.RecordTable.KEY_NOTES, comments);
        values.put(DBContract.RecordTable.KEY_REPEATING, period > 0 ? DBContract.REPEATING_ON : DBContract.REPEATING_OFF);

        String whereClause = DBContract.RecordTable._ID + " = ? AND " + DBContract.RecordTable.KEY_RECORD_TYPE + " = ?";
        String[] whereArgs = {rowID + "", DBContract.RECORD_EXPENSE + ""};

        int result = wdb.update(DBContract.RecordTable.TABLE_NAME, values, whereClause, whereArgs);
        return result == 1;
    }

    public Cursor getAllRecords() {
        String tableName = DBContract.RecordTable.TABLE_NAME;

        Cursor c = rdb.query(tableName, null, null, null, null, null, null, null);
        c.moveToFirst();
        return c;
    }

    public boolean unregisterAlarm(long rowID) {
        ContentValues values = new ContentValues();
        values.put(DBContract.RecordTable.KEY_REPEATING, DBContract.REPEATING_OFF);

        String whereClause = DBContract.RecordTable._ID + " = ?";
        String[] whereArgs = {rowID + ""};

        int result = wdb.update(DBContract.RecordTable.TABLE_NAME, values, whereClause, whereArgs);
        return result == 1;
    }
}
