package com.anikristo.android.personalbudget.misc;

import com.anikristo.android.personalbudget.database.DBContract;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO
 */
public class CategoryHolder {

    // CONSTANTS

    // Expense Categories TODO: R.string
    public static final Category FOOD = new Category("Food", 0);
    public static final Category COSMETICS = new Category("Cosmetics", 1);
    public static final Category TRANSPORT = new Category("Transport", 2);
    public static final Category CLOTHES = new Category("Clothes", 3);
    public static final Category STATIONERY = new Category("Stationery", 4);
    public static final Category UTILITIES = new Category("Utilities", 5);
    public static final Category TAXES = new Category("Taxes", 6);
    public static final Category OTHER = new Category("Other", 999);

    // Income Categories
    public static final Category SALARY = new Category("Salary", 0);
    public static final Category GIFT = new Category("Gift", 1);
    public static final Category AID = new Category("Financial Aid", 2);
    public static final Category SAVINGS = new Category("Savings", 3);

    // METHODS
    public static List<Category> getExpenseCategories() {
        ArrayList<Category> list = new ArrayList<>();
        list.add(FOOD);
        list.add(COSMETICS);
        list.add(TRANSPORT);
        list.add(CLOTHES);
        list.add(STATIONERY);
        list.add(UTILITIES);
        list.add(TAXES);
        list.add(OTHER);

        return list;
    }

    public static List<Category> getIncomeCategories() {
        ArrayList<Category> list = new ArrayList<>();
        list.add(SALARY);
        list.add(GIFT);
        list.add(AID);
        list.add(SAVINGS);
        list.add(OTHER);

        return list;
    }

    public static List<Category> getAllCategories(int recordType) {
        if (recordType == DBContract.RECORD_EXPENSE)
            return getExpenseCategories();
        else
            return getIncomeCategories();
    }

    public static Category getCategory(int position, int recordType) {
        if (recordType == DBContract.RECORD_INCOME) {
            switch (position) {
                case 0:
                    return SALARY;
                case 1:
                    return GIFT;
                case 2:
                    return AID;
                case 3:
                    return SAVINGS;
                default:
                    return OTHER;
            }
        } else if (recordType == DBContract.RECORD_EXPENSE) {
            switch (position) {
                case 0:
                    return FOOD;
                case 1:
                    return COSMETICS;
                case 2:
                    return TRANSPORT;
                case 3:
                    return CLOTHES;
                case 4:
                    return STATIONERY;
                case 5:
                    return UTILITIES;
                case 6:
                    return TAXES;
                default:
                    return OTHER;
            }
        }

        return OTHER;
    }

    public static Category getDefaultCategory() {
        return OTHER;
    }
}
