package com.anikristo.android.personalbudget.settings;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.anikristo.android.personalbudget.R;
import com.anikristo.android.personalbudget.dialogs.CurrencyPickerDialog;

/**
 * TODO
 */
public class SettingsActivity extends AppCompatActivity implements CurrencyPickerDialog.CurrencyDialogInterface {

    // DATA MEMBERS
    private Fragment frag;

    // METHODS
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar_settings));
        setTitle(R.string.title_activity_settings);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        frag = new SettingsFragment();
        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(R.id.pref_list, frag)
                .commit();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCurrencyPicked(int position) {
        ((SettingsFragment) frag).setCurrency(position);
    }
}
