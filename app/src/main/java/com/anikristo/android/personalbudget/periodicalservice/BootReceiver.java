package com.anikristo.android.personalbudget.periodicalservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import com.anikristo.android.personalbudget.database.DBContract;
import com.anikristo.android.personalbudget.database.DBHelper;
import com.anikristo.android.personalbudget.misc.Utils;

/**
 * TODO
 */
public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        // Loop through the records and add alarms
        for (Cursor c = new DBHelper(context).getAllRecords(); !c.isAfterLast(); c.moveToNext()) {

            // Getting initial properties
            int isRepeating = c.getInt(c.getColumnIndexOrThrow(DBContract.RecordTable.KEY_REPEATING));
            int period = c.getInt(c.getColumnIndexOrThrow(DBContract.RecordTable.KEY_PERIOD));

            // Periodical record
            if (isRepeating == DBContract.REPEATING_ON && period > 0) {

                // Get the properties
                long rowID = c.getLong(c.getColumnIndexOrThrow(DBContract.RecordTable._ID));
                double amount = c.getDouble(c.getColumnIndexOrThrow(DBContract.RecordTable.KEY_AMOUNT));
                int currency = c.getInt(c.getColumnIndexOrThrow(DBContract.RecordTable.KEY_CURRENCY));
                int day = c.getInt(c.getColumnIndexOrThrow(DBContract.RecordTable.KEY_DAY));
                int month = c.getInt(c.getColumnIndexOrThrow(DBContract.RecordTable.KEY_MONTH));
                int year = c.getInt(c.getColumnIndexOrThrow(DBContract.RecordTable.KEY_YEAR));
                int hour = c.getInt(c.getColumnIndexOrThrow(DBContract.RecordTable.KEY_HOUR));
                int minute = c.getInt(c.getColumnIndexOrThrow(DBContract.RecordTable.KEY_MINUTE));
                String notes = c.getString(c.getColumnIndexOrThrow(DBContract.RecordTable.KEY_NOTES));
                int category = c.getInt(c.getColumnIndexOrThrow(DBContract.RecordTable.KEY_CATEGORY));
                int recordType = c.getInt(c.getColumnIndexOrThrow(DBContract.RecordTable.KEY_RECORD_TYPE));

                // Create the bundle
                Bundle bundle = new Bundle();
                bundle.putLong(Utils.INTENT_EXTRA_ROW_ID, rowID);
                bundle.putDouble(Utils.INTENT_EXTRA_AMOUNT, amount);
                bundle.putInt(Utils.INTENT_EXTRA_CURRENCY, currency);
                bundle.putInt(Utils.INTENT_EXTRA_PERIOD, period);
                bundle.putInt(Utils.INTENT_EXTRA_DAY, day);
                bundle.putInt(Utils.INTENT_EXTRA_MONTH, month);
                bundle.putInt(Utils.INTENT_EXTRA_YEAR, year);
                bundle.putInt(Utils.INTENT_EXTRA_HOUR, hour);
                bundle.putInt(Utils.INTENT_EXTRA_MINUTE, minute);
                bundle.putString(Utils.INTENT_EXTRA_NOTES, notes);
                bundle.putInt(Utils.INTENT_EXTRA_CATEGORY, category);
                bundle.putInt(Utils.INTENT_EXTRA_RECORD_TYPE, recordType);


                Utils.registerAlarm(context, bundle);

            }
        }
    }
}
