package com.anikristo.android.personalbudget.records;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.anikristo.android.personalbudget.R;
import com.anikristo.android.personalbudget.database.DBContract;
import com.anikristo.android.personalbudget.dialogs.CategoryPickerDialog;
import com.anikristo.android.personalbudget.misc.Category;
import com.anikristo.android.personalbudget.misc.CategoryHolder;
import com.anikristo.android.personalbudget.misc.Currency;
import com.anikristo.android.personalbudget.misc.CurrencyHolder;
import com.anikristo.android.personalbudget.misc.Utils;
import com.anikristo.android.personalbudget.misc.interfaces.IMainFragment;

public class MainFragment extends Fragment implements IMainFragment, CategoryPickerDialog.CategoryPickerDialogListener {

    // WIDGETS
    private TextView amountTV;
    private EditText amountField;
    private TextView amountErrorTV;
    private TextView categoryTV;
    private EditText categoryValueTV;

    private int recordType;
    private Currency currency;
    private Category category;

    // pending info used in case the user is back to this activity from UNDO action
    // I used wrapper classes to be able to make them null
    private Double newAmount;
    private Integer newCurrency;
    private Integer newCategory;

    // METHODS
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_record_main, container, false);
        amountField = (EditText) v.findViewById(R.id.record_main_amount_et);
        amountTV = (TextView) v.findViewById(R.id.record_main_amount_tv);
        amountErrorTV = (TextView) v.findViewById(R.id.record_main_amount_error_tv);
        categoryTV = (TextView) v.findViewById(R.id.record_main_category_tv);
        categoryValueTV = (EditText) v.findViewById(R.id.record_main_categoryValue_tv);

        showAmountErrorTV(false);

        category = CategoryHolder.getDefaultCategory();
        categoryValueTV.setText(category.toString());
        categoryValueTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CategoryPickerDialog dialog = new CategoryPickerDialog();
                dialog.setListener(MainFragment.this);
                dialog.setRecordType(recordType);
                dialog.show(getFragmentManager(), "CATEGORY_DIALOG");
            }
        });

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        boolean useDefault = prefs.getBoolean(getString(R.string.pref_key_currency_mode), true);
        if (useDefault)
            currency = CurrencyHolder.getDefaultCurrency(getActivity());
        else
            currency = CurrencyHolder.getCurrency(prefs.getInt(getString(R.string.pref_key_currency_value), CurrencyHolder.getPosition(CurrencyHolder.UNDEFINED)));
        setHint();

        recordType = getActivity().getIntent().getIntExtra(Utils.INTENT_EXTRA_RECORD_TYPE, DBContract.RECORD_INCOME);
        stylizeAndPopulate();

        amountField.requestFocus();

        return v;
    }

    private void setHint() {
        amountTV.setText(getString(R.string.title_amount) + " (" + currency.getSymbol() + ")");
    }

    @Override
    public Category getCategory() {
        return category;
    }

    @Override
    public void setCategory(int category) {
        newCategory = category;
    }

    @Override
    public double getAmount() {
        if (amountField != null && !amountField.getText().toString().isEmpty())
            return Double.parseDouble(amountField.getText().toString());
        return 0;
    }

    @Override
    public void setAmount(double amount) {
        newAmount = amount;
    }

    @Override
    public Currency getCurrency() {
        return currency;
    }

    @Override
    public void setCurrency(int currency) {
        newCurrency = currency;

        if (isVisible()) {
            this.currency = CurrencyHolder.getCurrency(currency);
            setHint();
        }
    }

    @Override
    public void showAmountErrorTV(boolean show) {
        if (show)
            amountErrorTV.setVisibility(View.VISIBLE);
        else
            amountErrorTV.setVisibility(View.INVISIBLE);
    }

    @Override
    public void stylizeAndPopulate() {

        // Populating views
        if (newAmount != null)
            amountField.setText(newAmount.toString());

        if (newCurrency != null) {
            currency = CurrencyHolder.getCurrency(newCurrency);
            setHint();
        }

        if (newCategory != null) {
            category = CategoryHolder.getCategory(newCategory, recordType);
            categoryValueTV.setText(category.toString());
        }

        int color;
        if (recordType == DBContract.RECORD_EXPENSE) {
            color = getResources().getColor(R.color.accent_expenses_bright);
        } else {
            color = getResources().getColor(R.color.accent_incomes_bright);
        }

        amountTV.setTextColor(color);
        categoryTV.setTextColor(color);
    }

    @Override
    public boolean contains(int field) {
        if (field == AMOUNT)
            return getAmount() != 0;
        if (field == CURRENCY)
            return true;
        if (field == CATEGORY)
            return true;
        else
            throw new IllegalArgumentException("Field not found!");
    }

    public void focusAmountField() {
        if (amountField != null)
            amountField.requestFocus();
    }

    @Override
    public void onCategoryPicked(Category category) {
        this.category = category;
        this.categoryValueTV.setText(category.toString());
    }
}
